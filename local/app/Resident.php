<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

 
class Resident extends Model
{
  protected $table = 'resident';
  protected $primaryKey = 'id';

  public function user_office() {
    return $this->hasMany('App\User', 'office_id', 'id');
  }
}
