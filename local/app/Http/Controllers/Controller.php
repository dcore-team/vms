<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;

use Carbon\Carbon;
use DataTables;
use Validator; 
use Auth; 
use Hash;
use DB;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  public $today, $year, $round;
  
  public function __construct() {
    $this->middleware('auth');

    $this->year = (date('Y')+543);
    $this->today = date('Y-m-d');
    
    if( ($this->today >= date('Y-10-01')) || ($this->today <= date('Y-03-31')) ) {
      $this->round = 1;
    }
  
    if( ($this->today >= date('Y-04-01')) && ($this->today <= date('Y-09-30')) ) {
      $this->round = 2;
    }
  }

  public function data_employee($emp_id){
    return DB::table('users')
        ->leftjoin('position', 'position.position_code', '=', 'users.position_code')
        ->leftjoin('position_work', 'position_work.position_work_code', '=', 'position.position_work_code')
        ->leftjoin('position_type', 'position_type.position_type_code', '=', 'position.position_type_code')

        ->leftjoin('position_rank', 'position_rank.position_rank_code', '=', 'users.position_rank_code')

        ->leftjoin('office', 'office.office_code', '=', 'users.office_code')

        ->leftjoin('user_group', 'user_group.user_group_id', '=', 'users.user_group_id')
        ->join('total_weight', 'total_weight.id', '=', 'user_group.total_weight_id')

        ->select(
          'users.id',
          'users.name',
          'users.tel',
          'users.email',
          'users.username',
          'users.password',
          'users.user_group_id',
          'users.evaluate_group',
          'users.approve_group',
          'users.permission_group',
          'users.active',

          'user_group.user_group_id',
          'user_group.user_group_name',
          
          'total_weight.total_weight1',
          'total_weight.total_weight2',
            
          'position_type.position_type_code',
          'position_type.position_type_name',

          'position.position_work_code',
          'position_work.position_work_name',

          'users.position_rank_code',
          'position_rank.position_rank_name',
          
          'position.position_code',
          'position.position_name',

          'office.office_code',
          'office.office_name'
        )
        ->where(['users.id'=>$emp_id])
    ->first();
  }

  public function get_employee_evaluate($user_group_id){
    if(Auth::User()->user_group_id == '105') {
      $employees = DB::select("SELECT 
        users.id, 
        users.name,
        users.position_code,
        user_group.user_group_name,
        
        position.position_name,

        office.office_name,
        office_type.office_type_name
        FROM users
        LEFT JOIN position ON position.position_code = users.position_code 

        INNER JOIN (
          SELECT user_group.user_group_id, user_group.user_group_name
          FROM user_group 
          WHERE user_group.user_group_id IN ($user_group_id)
        ) 
        AS user_group ON user_group.user_group_id = users.user_group_id

        INNER JOIN (
          SELECT office.office_code, office.office_name, office.office_type_code
          FROM office 
          WHERE office.office_code =?
        ) 
        AS office ON office.office_code = users.office_code 										
                    
        LEFT JOIN office_type ON office_type.office_type_code = office.office_type_code 
        
        ORDER BY user_group.user_group_id ASC, office.office_code ASC, users.id ASC
      ", [Auth::User()->office_code]);
    }
    else {
      $employees = DB::select("SELECT 
        users.id, 
        users.name, 
        users.position_code,
        user_group.user_group_name,

        position.position_name,

        office.office_name

        FROM users
        LEFT JOIN position ON position.position_code = users.position_code
        LEFT JOIN office ON office.office_code = users.office_code 

        INNER JOIN (
          SELECT user_group.user_group_id, user_group.user_group_name
          FROM user_group 
          WHERE user_group.user_group_id IN ($user_group_id)
        ) 
        AS user_group ON user_group.user_group_id = users.user_group_id
        
        ORDER BY user_group.user_group_id ASC, office.office_code ASC, users.id ASC
      ", []);
    }

    return $employees;
  }

  public function get_employee_approve($user_group_id, $year,$round){
    return DB::select("SELECT 
      users.id, 
      users.name, 
      users.position_code,
      user_group.user_group_name,

      transaction_result.evaluate_id,
      transaction_result.palad_id,
      transaction_result.filter_id,
      transaction_result.nayok_id,

      approve_name(transaction_result.evaluate_id) as evaluate_name,
      approve_name(transaction_result.palad_id) as palad_name,
      approve_name(transaction_result.filter_id) as filter_name,
      approve_name(transaction_result.nayok_id) as nayok_name,

      position.position_name,

      office.office_name,

      transaction_result.year,
      transaction_result.round

      FROM users 
      LEFT JOIN position ON position.position_code = users.position_code
      LEFT JOIN office ON office.office_code = users.office_code 

      INNER JOIN transaction_result ON transaction_result.emp_id = users.id

      INNER JOIN (
        SELECT user_group.user_group_id, user_group.user_group_name
        FROM user_group 
        WHERE user_group.user_group_id IN ($user_group_id)
      ) 
      AS user_group ON user_group.user_group_id = users.user_group_id
      
      WHERE 
        transaction_result.year = $year
        AND transaction_result.round = $round
      ORDER BY user_group.user_group_id ASC, users.id ASC
    ", []);
  }

  public function getNotification($year,$round){

    $notify_score = DB::select("SELECT
        tr.score_result1,
        tr.score_result2,
        tr.year,
        tr.round,

        tr.status_notify,
        tr.status_accept

        FROM transaction_result tr
        WHERE tr.office_code =? 
          AND tr.emp_id =? 
          AND tr.year = $year
          AND tr.round = $round
          AND tr.status_notify = 1
          AND tr.status_accept IS NULL
    ", [Auth::user()->office_code, Auth::user()->id]);
    // return $notify_score;
    // dd($notify_score);
    if($notify_score != NULL){
      return response()->json(
        [
          'num_notify'=>count($notify_score),
          'status_notify'=>$notify_score[0]->status_notify,
          'status_accept'=>$notify_score[0]->status_accept
        ]
      );
    } else {
      return response()->json(
        [
          'num_notify'=>0,
          'status_notify'=>0,
          'status_accept'=>0
        ]
      );
    }
    
  }

  //ลบข้อมูล เพื่อทดสอบ
  public function DeleteData(Request $req){
    $indicator = DB::table('indicator')->where('user_id', $req->emp_id)->delete();
    $transaction = DB::table('transaction_choice')->where('user_id', $req->emp_id)->delete();
    // $choice = DB::table('indicator_choice')->where('id', $req->indicator_id)->delete();
    if($indicator == true) {
      // return 'ลบข้อมูลสำเร็จ';
      return response()->json([
        'msg'=>'ลบข้อมูลสำเร็จ'
      ]);
    } else { 
      return response()->json([
        'msg'=>'ลบข้อมูลไม่สำเร็จ'
      ]);
    }
  }

}
