<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use Hash;
use App\User;

class ProfileController extends Controller
{
  public function getProfile()
  {
    return view('profile.profile');
  }

  public function update(Request $req)
  {
    $profile = User::find(Auth::user()->id);
    $validate = Validator::make($req->all(), [
      'txtName' => 'required|string',
      'txtTel' => 'nullable|numeric|digits_between:9,10',
      'txtEmail' => 'nullable|email|unique:users,email,'.Auth::user()->id.',id',
    ], [ 
      'txtTel.numeric'=>'กรุณากรอกเบอร์โทรเป็นตัวเลข',
      'txtTel.digits_between'=>'กรุณากรอกเบอร์โทรให้ครบ',
      'txtEmail.unique'=>'มี E-Mail นี้ในระบบแล้ว',
      'txtEmail.email'=>'รูปแบบ E-Mail ไม่ถูกต้อง',
    ]);
    if ($validate->fails()) { //$validate->passes()
      return redirect()->back()->withErrors($validate->errors())->withInput($req->all());
    }
     
    $profile->name = $req->txtName;
    $profile->tel = $req->txtTel;
    $profile->email = $req->txtEmail;
    $profile->save();
    return redirect('profile')->with('success','บันทึกข้อมูลสำเร็จ');
  }



    
    public function addprofile(Request $request)
    {

        $validate = \Validator::make($request->all(), [
            'email' => 'nullable|email|unique:users,email,'.Auth::user()->id.',id',
            'username' => 'unique:users,username,' .Auth::user()->id.',id',
        ], [
            'username.unique'  =>  'username ต้องไม่ซ้ำ',
            'email.email' => 'รูปแบบการเขียน E-mail ไม่ถูกต้อง',
            'email.unique' => 'E-mail ต้องไม่ซ้ำ',
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate->errors())->withInput($request->all());
        }



        $data_users = users::find(Auth::user()->id);

        if ($request->hasFile('image')){

            $filename = Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path('/file'), $filename);
            $data_users->picture_user = $filename;
         }


        DB::update("UPDATE Employee SET Name = '$request->Name',
                                        ID_Employee = '$request->ID_Employee',
                                        Nick_name = '$request->Nick_name',
                                        gender = '$request->gender',
                                        Phone_Number = '$request->Phone_Number',
                                        Line_ID = '$request->Line_ID'
                                        WHERE Employee.ID_user = ?", [Auth::user()->id]);

        DB::update("UPDATE users SET username = '$request->username' , email = '$request->email' , picture_user = '$data_users->picture_user'
                                        WHERE users.id = ?", [Auth::user()->id]);

        return redirect('/profile');
    }

    public function changepassword(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'old_password'          => 'required',
            'password'              => 'required|min:4',
            'password_confirmation' => 'required|same:password'
        ], [
            'old_password.required'                 =>  'รหัสผ่านเก่าต้องไม่ว่าง',
            'password.required'                     =>  'รหัสผ่านใหม่ต้องไม่ว่าง',
            'password_confirmation.required'        =>  'ยืนยันรหัสผ่านใหม่ต้องไม่ว่าง',
            'password.min'                          =>  'รหัสผ่านใหม่ต้องไม่น้อยกว่า 4 ตัว',
            'password_confirmation.same'            =>  'รหัสผ่านใหม่ต้องตรงกับยืนยันรหัสผ่าน',
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate->errors())->withInput($request->all());
        }

        $data_users = new users();
        $data_users = users::find(Auth::user()->id);

        if (Hash::check($request->old_password, $data_users->password)) {
            $data_users->password = bcrypt($request->password);
            $data_users->save();
            return redirect('/profile');
           }

        return redirect()->back()->withErrors("รหัสผ่านไม่ถูกต้อง");
    }
}
