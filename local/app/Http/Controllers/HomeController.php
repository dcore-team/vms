<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Carbon\Carbon;
use DataTables;
use Validator; 
use Auth; 
use Hash;
use DB;

class HomeController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
  */
  public function index() {
    $year = $this->year;
    $round = $this->round;
    // dd(55);

    return view('home', []);
  }

}
