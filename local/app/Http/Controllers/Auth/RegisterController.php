<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:20', 'unique:users'], //addnew
            'password' => ['required', 'string', 'min:6', 'confirmed'],
						'email' => ['nullable', 'string', 'email', 'max:255', 'unique:users']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data 
     * @return \App\User
     */
    protected function create(array $data) //'field' => $data['txtName']
    {   //dd($data['txtName']);
        return User::create([
					'office_type'=>$data['ddlType'],
					'zone'=>$data['ddlZone'],
					'office'=>$data['txtOffice'],
          'username' => $data['username'], //addnew
          'password' => Hash::make($data['password']),
          'name' => $data['txtName'],
					'tel' => $data['txtTel'],
          'email' => $data['email'],
          'group' => $data['ddlGroup'],
          'status' => 1, //addnew
        //   'emp_permission' => implode(',', $data['chkPermission']), //addnew
        ]);
    }
}
