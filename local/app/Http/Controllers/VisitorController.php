<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Visitor;

use Carbon\Carbon;
use DataTables;
use Validator; 
use Auth; 
use Hash;
use DB;

class VisitorController extends Controller
{
  public function getVisitor(Request $req) {

    if ($req->ajax()) {
      $visitors = DB::table('visitor')
        // ->whereIn('users.user_group_id', [1,2,3,4,5,6])
        ->leftjoin('resident', 'resident.id', '=', 'visitor.resident_id')
        // ->leftjoin('office', 'office.office_id', '=', 'users.office_id')
        // ->leftjoin('office_type', 'office_type.office_type_id', '=', 'office.office_type_id')
        ->select(
          'visitor.id AS visitor_id',
          'visitor.name AS visitor_name',
          'visitor.date_in',
          'visitor.date_out',
          'visitor.image_people AS image_visitor',
          'visitor.image_car AS image_car_visitor',
         
          'resident.id AS resident_id',
          'resident.name AS resident_name',
          'resident.address AS resident_address'
        )
        ->orderByRaw('visitor.created_at ASC')
      ->get(); 
    }

    return Datatables::of($visitors)->toJson();
  }

  public function deleteVisitor($id) {

    $visitor_delete = Visitor::destroy($id);
    
    if($visitor_delete) {
      $msg = 'ลบข้อมูลสำเร็จ';
    } else {
      $msg = 'ลบข้อมูลไม่สำเร็จ';
    }

    return response()->json([
      'msg'=>$msg
    ]);

  }
  
}
