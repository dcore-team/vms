<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Checklogin
{ //NEW พี่ชาย
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()==null){return $next($request);}
        try{
            if(Auth::user()->id > 0)
            {
                // dd(Auth::user()->id);  
                return redirect('/home');
            }
        }catch (Exception $e){}
        
        return $next($request);
    }
}
