<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
      if ($this->isHttpException($exception)) {
        if ($exception->getStatusCode() == 403) {
          // dd($exception);
          //! Forbidden $exception->getMessage()
          return response()->view('error/err-all', 
            [
              "status_code"=>$exception->getStatusCode(),
              "msg_err"=>$exception->getMessage() ?$exception->getMessage() :"You don't have permission to access.",
              "bg_page"=>"bg-dark",
              "msg_err2"=>$exception->getMessage()=="ระบบยังไม่เปิดให้รายงานข้อมูล" ?"หากระบบเปิดให้รายงานจะแจ้งเวียนให้ทราบอีกครั้ง" :""
            ]
          );
        }

        if ($exception->getStatusCode() == 404) {
          // dd($exception);
          return response()->view('error/err-all', 
            [
              "status_code"=>$exception->getStatusCode(),
              "msg_err"=> 'The page you’re looking for was not found.',
              "msg_err2"=>"",
              "bg_page"=>"bg-dark"
            ]
          );
        }

        if ($exception->getStatusCode() == 500) {
          return response()->view('error/err-all', 
            [
                "status_code"=>$exception->getStatusCode(),
                "msg_err"=>"Internal server error.",
                "msg_err2"=>"",
                "bg_page"=>"bg-dark"
            ], 500
          );
        }
      }

      return parent::render($request, $exception);
    }
}
