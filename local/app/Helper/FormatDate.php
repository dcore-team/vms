<?php
function th_date($time){   // 19 ธันวาคม 2556 global $thai_day_arr,$thai_month_arr;
  $arr_month = array(
    "0"=>"",
    "1"=>"มกราคม",
    "2"=>"กุมภาพันธ์",
    "3"=>"มีนาคม",
    "4"=>"เมษายน",
    "5"=>"พฤษภาคม",
    "6"=>"มิถุนายน",
    "7"=>"กรกฎาคม",
    "8"=>"สิงหาคม",
    "9"=>"กันยายน",
    "10"=>"ตุลาคม",
    "11"=>"พฤศจิกายน",
    "12"=>"ธันวาคม"
  );
  $d = strtotime($time); //j = วันที่เลขตัวเดียว j = เดือนเลขตัวเดียว
  $th_date = date('j',$d).' '.$arr_month[date('n',$d)].' '.(date('Y',$d)+543);
  return $th_date;
}

function th_date_short($time){   // 19  ธ.ค. 2556 date('Y-m-d');
  $arr_month = array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
  $d = strtotime($time);
  $th_date_short = date('j',$d).' '.$arr_month[date('n',$d)].' '.(date('Y',$d)+543);
  return $th_date_short;
}

function th_datetime_short($time){   // 19  ธ.ค. 2556 date('Y-m-d');
  $arr_month = array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
  $d = strtotime($time);
  $th_datetime_short = date('j',$d).' '.$arr_month[date('n',$d)].' '.(date('Y',$d)+543).' '.(date('H:i',$d));
  return $th_datetime_short;
}

function th_day($time){
  $arr_day = array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
  $d = strtotime($time);
  $th_day = 'วัน'.$arr_day[date("w",$d)];
  return $th_day;
}

function code_to_country($code){
  $code = strtoupper($code);
  $arr_list = array(
      'AF' => 'Afghanistan',
      'AX' => 'Aland Islands',
      'AL' => 'Albania',
      'DZ' => 'Algeria',
      'AS' => 'American Samoa',
      'AD' => 'Andorra',
      'AO' => 'Angola',
      'AI' => 'Anguilla',
      'AQ' => 'Antarctica',
      'AG' => 'Antigua and Barbuda',
      'AR' => 'Argentina',
      'AM' => 'Armenia',
      'AW' => 'Aruba',
      'AU' => 'Australia',
      'AT' => 'Austria',
      'AZ' => 'Azerbaijan',
      'BS' => 'Bahamas the',
      'BH' => 'Bahrain',
      'BD' => 'Bangladesh',
      'BB' => 'Barbados',
      'BY' => 'Belarus',
      'BE' => 'Belgium',
      'BZ' => 'Belize',
      'BJ' => 'Benin',
      'BM' => 'Bermuda',
      'BT' => 'Bhutan',
      'BO' => 'Bolivia',
      'BA' => 'Bosnia and Herzegovina',
      'BW' => 'Botswana',
      'BV' => 'Bouvet Island (Bouvetoya)',
      'BR' => 'Brazil',
      'IO' => 'British Indian Ocean Territory (Chagos Archipelago)',
      'VG' => 'British Virgin Islands',
      'BN' => 'Brunei Darussalam',
      'BG' => 'Bulgaria',
      'BF' => 'Burkina Faso',
      'BI' => 'Burundi',
      'KH' => 'Cambodia',
      'CM' => 'Cameroon',
      'CA' => 'Canada',
      'CV' => 'Cape Verde',
      'KY' => 'Cayman Islands',
      'CF' => 'Central African Republic',
      'TD' => 'Chad',
      'CL' => 'Chile',
      'CN' => 'China',
      'CX' => 'Christmas Island',
      'CC' => 'Cocos (Keeling) Islands',
      'CO' => 'Colombia',
      'KM' => 'Comoros the',
      'CD' => 'Congo',
      'CG' => 'Congo the',
      'CK' => 'Cook Islands',
      'CR' => 'Costa Rica',
      'CI' => 'Cote d\'Ivoire',
      'HR' => 'Croatia',
      'CU' => 'Cuba',
      'CY' => 'Cyprus',
      'CZ' => 'Czech Republic',
      'DK' => 'Denmark',
      'DJ' => 'Djibouti',
      'DM' => 'Dominica',
      'DO' => 'Dominican Republic',
      'EC' => 'Ecuador',
      'EG' => 'Egypt',
      'SV' => 'El Salvador',
      'GQ' => 'Equatorial Guinea',
      'ER' => 'Eritrea',
      'EE' => 'Estonia',
      'ET' => 'Ethiopia',
      'FO' => 'Faroe Islands',
      'FK' => 'Falkland Islands (Malvinas)',
      'FJ' => 'Fiji the Fiji Islands',
      'FI' => 'Finland',
      'FR' => 'France, French Republic',
      'GF' => 'French Guiana',
      'PF' => 'French Polynesia',
      'TF' => 'French Southern Territories',
      'GA' => 'Gabon',
      'GM' => 'Gambia the',
      'GE' => 'Georgia',
      'DE' => 'Germany',
      'GH' => 'Ghana',
      'GI' => 'Gibraltar',
      'GR' => 'Greece',
      'GL' => 'Greenland',
      'GD' => 'Grenada',
      'GP' => 'Guadeloupe',
      'GU' => 'Guam',
      'GT' => 'Guatemala',
      'GG' => 'Guernsey',
      'GN' => 'Guinea',
      'GW' => 'Guinea-Bissau',
      'GY' => 'Guyana',
      'HT' => 'Haiti',
      'HM' => 'Heard Island and McDonald Islands',
      'VA' => 'Holy See (Vatican City State)',
      'HN' => 'Honduras',
      'HK' => 'Hong Kong',
      'HU' => 'Hungary',
      'IS' => 'Iceland',
      'IN' => 'India',
      'ID' => 'Indonesia',
      'IR' => 'Iran',
      'IQ' => 'Iraq',
      'IE' => 'Ireland',
      'IM' => 'Isle of Man',
      'IL' => 'Israel',
      'IT' => 'Italy',
      'JM' => 'Jamaica',
      'JP' => 'Japan',
      'JE' => 'Jersey',
      'JO' => 'Jordan',
      'KZ' => 'Kazakhstan',
      'KE' => 'Kenya',
      'KI' => 'Kiribati',
      'KP' => 'Korea',
      'KR' => 'Korea',
      'KW' => 'Kuwait',
      'KG' => 'Kyrgyz Republic',
      'LA' => 'Lao',
      'LV' => 'Latvia',
      'LB' => 'Lebanon',
      'LS' => 'Lesotho',
      'LR' => 'Liberia',
      'LY' => 'Libyan Arab Jamahiriya',
      'LI' => 'Liechtenstein',
      'LT' => 'Lithuania',
      'LU' => 'Luxembourg',
      'MO' => 'Macao',
      'MK' => 'Macedonia',
      'MG' => 'Madagascar',
      'MW' => 'Malawi',
      'MY' => 'Malaysia',
      'MV' => 'Maldives',
      'ML' => 'Mali',
      'MT' => 'Malta',
      'MH' => 'Marshall Islands',
      'MQ' => 'Martinique',
      'MR' => 'Mauritania',
      'MU' => 'Mauritius',
      'YT' => 'Mayotte',
      'MX' => 'Mexico',
      'FM' => 'Micronesia',
      'MD' => 'Moldova',
      'MC' => 'Monaco',
      'MN' => 'Mongolia',
      'ME' => 'Montenegro',
      'MS' => 'Montserrat',
      'MA' => 'Morocco',
      'MZ' => 'Mozambique',
      'MM' => 'Myanmar',
      'NA' => 'Namibia',
      'NR' => 'Nauru',
      'NP' => 'Nepal',
      'AN' => 'Netherlands Antilles',
      'NL' => 'Netherlands the',
      'NC' => 'New Caledonia',
      'NZ' => 'New Zealand',
      'NI' => 'Nicaragua',
      'NE' => 'Niger',
      'NG' => 'Nigeria',
      'NU' => 'Niue',
      'NF' => 'Norfolk Island',
      'MP' => 'Northern Mariana Islands',
      'NO' => 'Norway',
      'OM' => 'Oman',
      'PK' => 'Pakistan',
      'PW' => 'Palau',
      'PS' => 'Palestinian Territory',
      'PA' => 'Panama',
      'PG' => 'Papua New Guinea',
      'PY' => 'Paraguay',
      'PE' => 'Peru',
      'PH' => 'Philippines',
      'PN' => 'Pitcairn Islands',
      'PL' => 'Poland',
      'PT' => 'Portugal, Portuguese Republic',
      'PR' => 'Puerto Rico',
      'QA' => 'Qatar',
      'RE' => 'Reunion',
      'RO' => 'Romania',
      'RU' => 'Russian Federation',
      'RW' => 'Rwanda',
      'BL' => 'Saint Barthelemy',
      'SH' => 'Saint Helena',
      'KN' => 'Saint Kitts and Nevis',
      'LC' => 'Saint Lucia',
      'MF' => 'Saint Martin',
      'PM' => 'Saint Pierre and Miquelon',
      'VC' => 'Saint Vincent and the Grenadines',
      'WS' => 'Samoa',
      'SM' => 'San Marino',
      'ST' => 'Sao Tome and Principe',
      'SA' => 'Saudi Arabia',
      'SN' => 'Senegal',
      'RS' => 'Serbia',
      'SC' => 'Seychelles',
      'SL' => 'Sierra Leone',
      'SG' => 'Singapore',
      'SK' => 'Slovakia (Slovak Republic)',
      'SI' => 'Slovenia',
      'SB' => 'Solomon Islands',
      'SO' => 'Somalia, Somali Republic',
      'ZA' => 'South Africa',
      'GS' => 'South Georgia and the South Sandwich Islands',
      'ES' => 'Spain',
      'LK' => 'Sri Lanka',
      'SD' => 'Sudan',
      'SR' => 'Suriname',
      'SJ' => 'Svalbard & Jan Mayen Islands',
      'SZ' => 'Swaziland',
      'SE' => 'Sweden',
      'CH' => 'Switzerland, Swiss Confederation',
      'SY' => 'Syrian Arab Republic',
      'TW' => 'Taiwan',
      'TJ' => 'Tajikistan',
      'TZ' => 'Tanzania',
      'TH' => 'ประเทศไทย',
      'TL' => 'Timor-Leste',
      'TG' => 'Togo',
      'TK' => 'Tokelau',
      'TO' => 'Tonga',
      'TT' => 'Trinidad and Tobago',
      'TN' => 'Tunisia',
      'TR' => 'Turkey',
      'TM' => 'Turkmenistan',
      'TC' => 'Turks and Caicos Islands',
      'TV' => 'Tuvalu',
      'UG' => 'Uganda',
      'UA' => 'Ukraine',
      'AE' => 'United Arab Emirates',
      'GB' => 'United Kingdom',
      'US' => 'United States of America',
      'UM' => 'United States Minor Outlying Islands',
      'VI' => 'United States Virgin Islands',
      'UY' => 'Uruguay, Eastern Republic of',
      'UZ' => 'Uzbekistan',
      'VU' => 'Vanuatu',
      'VE' => 'Venezuela',
      'VN' => 'Vietnam',
      'WF' => 'Wallis and Futuna',
      'EH' => 'Western Sahara',
      'YE' => 'Yemen',
      'ZM' => 'Zambia',
      'ZW' => 'Zimbabwe'
    );
  if( !$arr_list[$code] ){
    return $code;
  } else {
    return $arr_list[$code];
  }
}

  ///////////////////////// Insert Chay ////////////////////
  function convert_shortdate($date) // 19 ธ.ค. 2018 -> 2018-12-19
  {
    $year_ = '1';
    $month_ = '1';
    $day_ = '1';
    try
    {
      $year_ = substr($date, -4); /////////// dont explode ''
      $day_ = mb_substr($date,0,2,'UTF-8');
      $month_ = mb_substr($date,3,4,'UTF-8');
      if(!is_numeric($day_))
      {
        $day_ = mb_substr($date,0,1,'UTF-8');
        $month_ = mb_substr($date,2,4,'UTF-8');
      }
      $month_int = searchintdate($month_);
    }
      catch (Exception $e) {}
  
    return $year_.'-'.$month_int.'-'.$day_;
  }
    
  function searchintdate($namemonth)
  {
    $result = 0;
    $thai_month_arr_short=array(
      "0"=>"",
      "1"=>"ม.ค.",
      "2"=>"ก.พ.",
      "3"=>"มี.ค.",
      "4"=>"เม.ย.",
      "5"=>"พ.ค.",
      "6"=>"มิ.ย.",
      "7"=>"ก.ค.",
      "8"=>"ส.ค.",
      "9"=>"ก.ย.",
      "10"=>"ต.ค.",
      "11"=>"พ.ย.",
      "12"=>"ธ.ค."
      );
      $month_int = array_search($namemonth,$thai_month_arr_short);
      if($month_int>0){$result = $month_int;}
      
      return $result;
  }
  
  function convert_shortdatetype2($date) // 04-ก.พ.-2562 -> 2018-12-19
  {
  
    $year_ = '';
    $month_ = '';
    $day_ = '';
    $result="";
    try
    {
      $value_=explode("-",$date); 
      if(count($value_)>0)
      {
           $day_ = $value_[0];
           $month_ = $value_[1];
           $month_int = searchintdate($month_);
             
         $year_  = (int)$value_[2]-543;
        //$year_ = $value_[2];
        $result = $year_.'-'.$month_int.'-'.$day_;
      }
      // if(count($value_)>0)
      // {
      //   $day_ = $value_[0];
      //   $month_ = $value_[1];
      //   $result = $day_;
      //   // $month_int = array_search($month_,$thai_month_arr_short);
      //   // $year_  = (int)$value_[2]-543;
      //   // $result = $year_.'-'.$month_int.'-'.$day_;
      // }else {
      //   $result = $date;
      // }
    }
    catch (Exception $e) {}
  
    return $result;
  }
  function thai_date_and_time_short($time){   // 19  ธ.ค. 2556 10:10:4
      global $thai_day_arr,$thai_month_arr_short;
      $thai_date_return = date("j",$time);
      $thai_date_return.=' '.$thai_month_arr_short[date("n",$time)];
      $thai_date_return.=' '.(date("Y",$time)+543);
      $thai_date_return.=' '.date("H:i:s",$time);
      return $thai_date_return;
    }
  
    $thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
  
    function convert_thaidate2year($date) //// 19/12/61 -> 2019-12-19
    {
      $str_ = explode('/',$date);
      $newstr_ = "";
      if(count($str_)>2)
      {
        $date_old = '25'.$str_[2];
        $date_new = (int)$date_old-543;
        $newstr_ = $date_new.'-'.$str_[1].'-'.$str_[0];
      }
      return $newstr_;
    }
    function convert_yeardate($date) // 19/12/2561 -> 2019-12-19
    {
      $str_ = explode('/',$date);
       $newstr_ = "";
      if(count($str_)>2)
      {
        $a = (int)$str_[2]-543;
        $newstr_ = $a.'-'.$str_[1].'-'.$str_[0];
      }
      return $newstr_;
    }
    function texttodouble($string_)
    {
       $str_ = explode(',',$string_);
       $newstr_ = "";
      for($i=0;$i<count($str_);$i++)
      {
        $newstr_ = $newstr_.$str_[$i];
      }
        return $newstr_;
    }
    function totalbilltointeger($string_)
    {
      $str_ = explode(' ',$string_);
        return $str_[0];
    }
  ///////////////////////// End Insert Chay ////////////////////
?>
