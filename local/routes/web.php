<?php
Auth::routes();
 
Route::get('/', function () { return view('auth.login'); })->middleware('checklogin');
Route::get('/developer-', function () { return view('developer'); })->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
//!
//?
//* 
//todo


//todo สำหรับ Reception
//? ข้อมูลลูกบ้าน
Route::get('setting/resident', function () { return view('resident/resident-list'); })->middleware('auth');
Route::get('setting/get-resident', 'SettingController@getResident')->middleware('auth');
Route::get('setting/resident-create', 'SettingController@createResident')->middleware('auth');
Route::post('setting/resident-insert', 'SettingController@insertResident')->middleware('auth');
Route::get('setting/{id}/resident-edit', 'SettingController@editResident')->middleware('auth');
Route::post('setting/{id}/resident-update', 'SettingController@updateResident')->middleware('auth');
Route::post('setting/{id}/resident-delete', 'SettingController@deleteResident')->middleware('auth');

//? ข้อมูลผู้เข้าพบ
Route::get('visitor', function () { return view('visitor/visitor-list'); })->middleware('auth');
Route::get('visitor/get-visitor', 'VisitorController@getVisitor')->middleware('auth');
Route::post('visitor/{id}/delete-visitor', 'VisitorController@deleteVisitor')->middleware('auth');