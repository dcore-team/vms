<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold' => $rootDir . '\lib\fonts\ZapfDingbats',
    'italic' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold_italic' => $rootDir . '\lib\fonts\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '\lib\fonts\Symbol',
    'bold' => $rootDir . '\lib\fonts\Symbol',
    'italic' => $rootDir . '\lib\fonts\Symbol',
    'bold_italic' => $rootDir . '\lib\fonts\Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSans-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSans-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSans-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSansMono-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '\lib\fonts\DejaVuSerif-Italic',
    'normal' => $rootDir . '\lib\fonts\DejaVuSerif',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir . '\2b93c0580452a466f02911db3bb3e76a',
  ),
  'thsarabun' => array(
    'normal' => $fontDir . '\010d9cfc58bbe15c2ae7b1a0f9b8edba',
    'bold' => $fontDir . '\0fbe9114c8b3c22435d136b8f081a207',
  ),
  'thsarabunnew' => array(
    'normal' => $fontDir . '\ba31c42a386e3be9becda2dfffc5f7a5',
  ),
) ?>