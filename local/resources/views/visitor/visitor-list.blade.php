@extends('layouts.master')

@section('title', 'ข้อมูลผู้เข้าพบ')
@section('css')
  <style>

  </style>
@stop
@section('content')
<div class="row"> 
  <div class="col-md-12">
		<h2 class="form-inline">
        ข้อมูลผู้เข้าพบ
    </h2>
  </div>
  

 
</div>

<div class="row">
	<div class="col-md-12">
    <div class="card grid-margin">
      <div class="card-body">
        <h4 class="font-weight-bold">
            <u>
              
            </u>
        </h4>
        
        <?php
          // Multidimensional array
          /*
          $superheroes = array(
              "spider-man" => array(
                  "name" => "Peter Parker",
                  "email" => "peterparker@mail.com",
              ),
              "super-man" => array(
                  "name" => "Clark Kent",
                  "email" => "clarkkent@mail.com",
              ),
              "iron-man" => array(
                  "name" => "Harry Potter",
                  "email" => "harrypotter@mail.com",
              )
          );
          
          // Printing all the keys and values one by one
          $keys = array_keys($superheroes);
          for($i = 0; $i < count($superheroes); $i++) {
              echo $keys[$i] . "{<br>";
              foreach($superheroes[$keys[$i]] as $key => $value) {
                  echo $key . " : " . $value . "<br>";
              }
              echo "}<br>";
          }
          */

          $arr = array(
            array(
              "product" => "AA",
              "price" => "100",
            ),
            array(
              "product" => "BB",
              "price" => "200",
            ),
            array(
              "product" => "CC",
              "price" => "300",
            )
          );

          // echo "<pre>";
          //   print_r($arr);
          // echo "</pre>";

          // for ($row = 0; $row < count($arr); $row++) {
          foreach($arr as $row=>$val) {
            // echo "<u>".$row."</u><br>";
            foreach($arr[$row] as $key => $value) {
              // echo $key."=>".$value."<br>";
            }

          }
        ?>

        <!-- <div id="lightgallery" class="row lightGallery">
          <a href="{{ asset('assets/images/samples/1280x768/1.jpg') }}" class="image-tile">
            <img src="{{ asset('assets/images/samples/300x300/1.jpg') }}" alt="image small"> </a>
          <a href="{{ asset('assets/images/samples/1280x768/2.jpg') }}" class="image-tile">
            <img src="{{ asset('assets/images/samples/300x300/2.jpg') }}" alt="image small"> </a>
        </div> -->
        
        <table class="tbl-indicator table-bordered tbl-visitor">
          <thead class="text-center">
            <tr>
              <th width="5%">ลำดับ</th>
              <th width="%">ชื่อผู้เข้าพบ</th>
              <th width="%">เวลาเข้า</th>
              <th width="%">เวลาออก</th>
              <th width="%">ชื่อลูกบ้าน</th>
              <th width="%">ที่อยู่ลูกบ้าน</th>
              <th width="10%">ดำเนินการ</th>
            </tr>
          </thead>

          <tbody>
            
          </tbody>
        </table>

      </div>
    </div>
	</div>
</div>



<div class="modal fade modal-visitor">
  <div class="modal-dialog modal-">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">
          <i class="fa fa-user-clock"></i> ข้อมูลผู้เข้าพบ
        </h3>

        <button class="btn btn-icons btn-rounded btn-outline-danger" data-dismiss="modal">
          <i class="fa fa-times"></i>
        </button>
      </div>

      <form id="frmVisitor">
        {{ csrf_field() }}
        <input type="hidden" name="visitor_id" value="">

        <div class="modal-body pb-4 pt-1">
          <div class="row mb-2">
            <div class="col-md-12">
@while (กาดวายลูป)
    
@endwhile
              <div id="visitor_image" class="row lightGallery">
              </div>

              <div id="visitor_car" class="row lightGallery">
              </div>

              <div id="visitor_data"></div>
            </div>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  var visitor_main = (function () {
    var datatable_visitor1;

    var datatable_visitor = function () {
      datatable_visitor1 = $('.tbl-visitor').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        // order: [[ 1, 'asc' ]],
        iDisplayLength: 20,
        aLengthMenu: [
          [20, 30, 40, 50, -1], 
          [20, 30, 40, 50, "ทั้งหมด"]
        ], 
        language: {
          search: "" 
        },
  
        ajax: {
          type: "GET",
          url: "{{ url('visitor/get-visitor') }}",
          data: function (d) {
            // d.year = $('select[name=ddlYear]').val();
            // d.round = $('select[name=ddlRound]').val();
            // console.log(d);
          },
        },
        columns: [
          { data: 'visitor_id'},
          { data: 'visitor_name' },
          { data: 'date_in' },
          { data: 'date_out' },
          { data: 'resident_name' },
          { data: 'resident_address' },
          { data: 'visitor_id' }
        ],
        columnDefs: [
          { 
            targets: 0, "className":"text-center",
            render: function (data, type, row, meta) {
              return (meta.row + meta.settings._iDisplayStart + 1);
            } 
          },

          {
            targets: -1,
            searchable: false,
            className: 'text-center',
  
            render: function (data, type, row, meta) { //`
              return '<button class="btn btn-icons btn-primary get-visitor" onclick="getVisitor('+data+')" title="ดู"><i class="fa fa-eye"></i></button>\
                <button class="btn btn-icons btn-danger delete-visitor" onclick="deleteVisitor('+data+')" title="ลบข้อมูล"><i class="fa fa-trash-alt"></i></button>';
            }
          }
        ],
      });
    }

    getVisitor = function(id) {
      console.log(id);
    }
    $(".tbl-visitor").on("click", ".get-visitor", function () {
      var visitor = datatable_visitor1.row( $(this).parents('tr') ).data();
      // console.log(visitor); 

      $('input[name="visitor_id"]').val(visitor.visitor_id);
      // let visitor_data = '';
      // visitor.forEach(element => {
      //   visitor_data += element;
      // });
      arr_key = [];
      arr_value = [];
      $.each(visitor, function (key, value) { 
        arr_key.push(
          {
            title: key, 
            link:  value
          }
        );
        arr_value.push(value);
      });
      console.log(arr_key);
      // $('.modal-visitor #visitor_data').html(arr_key.join( "<br>"));

      $("#visitor_image").lightGallery();
      $("#visitor_image").data('lightGallery').destroy(true);
      // $('#divPhoto').empty();

      if (visitor.image_visitor!= null) {
        $("#visitor_image").html(`
          <a href="{{ asset('local/public/visitor/people/${visitor.image_visitor}') }}" class="image-tile mb-2" >
            <img  align="middle" src="{{ asset('local/public/visitor/people/${visitor.image_visitor}') }}" alt="${visitor.image_visitor}">
          </a>
        `);
        $("#visitor_image").lightGallery();
      } else {
        $("#divPhoto").html(`
          <div class="col-md-12">
            <label style="color: blue;">ไม่มีภาพถ่าย</label>
          </div>
        `);
      }

      

      $("#visitor_car").lightGallery();
      $("#visitor_car").data('lightGallery').destroy(true);
      if (visitor.image_car_visitor!= null) {
        $("#visitor_car").html(`
          <a href="{{ asset('local/public/visitor/car/${visitor.image_car_visitor}') }}" class="image-tile mb-2" >
            <img  align="middle" src="{{ asset('local/public/visitor/car/${visitor.image_car_visitor}') }}" alt="${visitor.image_car_visitor}">
          </a>
        `);
        $("#visitor_car").lightGallery();
      } else {
        $("#divPhoto").html(`
          <div class="col-md-12">
            <label style="color: blue;">ไม่มีภาพถ่าย</label>
          </div>
        `);
      }

      $('.modal-visitor').modal('show');
    });
    

    deleteVisitor = function(id) {
      console.log(id);
    }
    $(".tbl-visitor").on("click", ".delete-visitor", function () {
      
      var visitor = datatable_visitor1.row( $(this).parents('tr') ).data();
      console.log(visitor);

      if(confirm('ต้องการลบข้อมูลหรือไม่ ?')) {
        $.ajax({
          type: "POST",
          url: `{{ url('visitor/${visitor.visitor_id}/delete-visitor') }}`,
          // data: {user_id: id},
          
          success: function (res) {
            console.log(res);
            alert(res.msg)
            datatable_visitor1.ajax.reload();
          },
          error: function (err) {
            console.log(err.responseJSON);
          }
        });
      }

    });

    return {
			init: function () {
				datatable_visitor();
			}
		}
  })()

  $(document).ready(function () {
		visitor_main.init();
	});
</script>
@endsection
