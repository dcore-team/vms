@extends('layouts.master')

@section('title', 'รายงานตัวชี้วัด')
@section('css')
  <style>
    
  </style>
@stop
@section('content')
<div class="row"> 
  <div class="col-md-12">
    <h2 class="form-inline">ตรวจสอบข้อมูลดิบรายงานตัวชี้วัด
    </h2>
  </div>
</div> 
<div class="row"> 
	<div class="col-md-12">
    <h4>ตัวชี้วัด {{ $indicators->ind_detail }}</h4>
    {{-- @foreach ($choices as $choice)
      <p>{{ $choice->ic_detail }}</p>
    @endforeach --}}
    <table class="tbl-indicator table-bordered">
      <thead class="text-center">
        <tr>
          <th width="30%">หน่วยงาน</th>
          <th width="65%">ข้อมูลการรายงานแต่ละเดือน</th>
        </tr>
      </thead>  
      <tbody>
      @if(count($office) > 0)
        @foreach ($office as $i=>$out_office)
          <tr class="bg-secondary">
            <td colspan="2">{{$out_office->office_name}}</td>
          </tr>
          @foreach ($transaction_month as $tm)
            @if ($out_office->office_id == $tm->office_id)
              <tr>
                <td class="pl-4">
                  {{ $tm->ic_id }}
                  {{ $tm->ic_detail }} 
                </td>
                <td>
                  <table class="table-bordered" width="100%">
                    <tr class="text-center">
                      <th width="8.33%">ม.ค.</th>
                      <th width="8.33%">ก.พ</th>
                      <th width="8.33%">มี.ค.</th>
                      <th width="8.33%">เม.ย.</th>
                      <th width="8.33%">พ.ค.</th>
                      <th width="8.33%">มิ.ย.</th>

                      <th width="8.33%">ก.ค.</th>
                      <th width="8.33%">ส.ค.</th>
                      <th width="8.33%">ก.ย.</th>
                      <th width="8.33%">ต.ค.</th>
                      <th width="8.33%">พ.ย.</th>
                      <th width="8.33%">ธ.ค.</th>
                    </tr>
                    <tr>
                      <td>{{ $tm->tm_answer1 }}</td>
                      <td>{{ $tm->tm_answer2 }}</td>
                      <td>{{ $tm->tm_answer3 }} </td>
                      <td>{{ $tm->tm_answer4 }}</td>
                      <td>{{ $tm->tm_answer5 }}</td>
                      <td>{{ $tm->tm_answer6 }}</td>

                      <td>{{ $tm->tm_answer7 }}</td>
                      <td>{{ $tm->tm_answer8 }} </td>
                      <td>{{ $tm->tm_answer9 }}</td>
                      <td>{{ $tm->tm_answer10 }}</td>
                      <td>{{ $tm->tm_answer11 }}</td>
                      <td>{{ $tm->tm_answer12 }}</td>
                    </tr>

                    {{-- <tr class="text-center">
                      <th>มกราคม</th>
                      <th>กุมภาพันธ์</th>
                      <th>มีนาคม</th>
                      <th>เมษายน</th>
                      <th>พฤษภาคม</th>
                      <th>มิถุนายน</th>
                      <th>กรกฎาคม</th>
                      <th>สิงหาคม</th>
                      <th>กันยายน</th>
                      <th>ตุลาคม</th>
                      <th>พฤศจิกายน</th>
                      <th>ธันวาคม</th>
                    </tr>
                    <tr>
                      
                    </tr> --}}
                  </table> 
                </td>
              </tr>
            @endif
          @endforeach
        @endforeach
      @else
        <tr>
          <th colspan="6" class="text-center">ยังไม่มีข้อมูลการรายงาน</th>
        </tr>
      @endif
      </tbody>
    </table> 
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">  
  function getYear(year) {
    if(year==0){ //console.log(1);
      window.location.href='{{url('indicator-month')}}';
    } else if(year!=0){ //console.log(2);
      window.location.href='{{url('indicator-month-year')}}'+'/'+year;
    }
  }
</script>
@endsection