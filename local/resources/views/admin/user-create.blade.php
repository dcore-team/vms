@extends('layouts.master')

@section('title', 'เพิ่มข้อมูลผู้ใช้งาน')
@section('css')
  <style media="screen">
    .btnDeleteChoice{
      cursor: pointer;
    }
  </style>
@stop
@section('content')
  <div class="row">
    <div class="col-md-12">
      <h2 class="form-inline">เพิ่มข้อมูลผู้ใช้งาน
      </h1>
    </div>

  </div>

  <div class="row"> 
    <div class="col-md-12">

      <form id="frmInsert" action="{{ url('admin/create/user') }}" method="POST" enctype="multipart/form-data"> 
      {{-- <form id="frmInsert" method="post"> --}}
        @csrf
        <div class="card">
          <div class="card-body pb-3">

            <div class="accordion basic-accordion" id="accordion" role="tablist">
              <div class="card">
                <div class="card-header" role="tab">
                  <h6 class="mb-0">
                    <a data-toggle="collapse" href="#div-indicator" aria-expanded="true" aria-controls="div-indicator">
                      {{-- <i class="card-icon fa fa-file-invoice"></i> --}}
                      <i class="card-icon mdi mdi-checkbox-marked-circle-outline"></i><u>ข้อมูลผู้ใช้งาน</u>
                    </a>
                  </h6>
                </div>

                <div id="div-indicator" class="collapse show" role="tabpanel" data-parent="#accordion">
                  <div class="card-body">
                    
                      <div class="row">
                        <div class="col-md-4 form-group">
                          <label>ประเภทหน่วยงาน</label>
                          <select name="ddlTypeOffice" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlTypeOffice">
                            <option value="0">เลือกประเภทหน่วยงาน</option>
                            @foreach ($office_type as $type)
                              <option value="{{ $type->office_type_id }}" >
                                {{ $type->office_type_name }}
                              </option>
                            @endforeach
                          </select>
                        </div>

                        <div class="col-md-4 form-group">
                          <label>หน่วยงาน</label>
                          <select name="ddlOffice" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlOffice">
                            <option value="0">เลือกหน่วยงาน</option>
                          </select>
                        </div>
          
                        <div class="col-md-4 form-group">
                          <label>หน้าที่</label>
                          <select name="ddlGroup" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlGroup">
                            <option value="0">เลือกหน้าที่ (user_group)</option>
                          </select>
                        </div>
                      </div>
          
                      <div class="row">
                        <div class="col-md-4 form-group">
                          <label>Username</label>
                          <input type="text" name="txtUser" onfocusout="Validate(this.name, this.tagName)" class="form-control txtUser">
                        </div>
                        
                      </div>

                      <div class="row">
                        <div class="col-md-3 form-group">
                          <label>ชื่อ</label>
                          <input type="text" name="txtName" onfocusout="Validate(this.name, this.tagName)" class="form-control txtName">
                        </div>

                        <div class="col-md-3 form-group">
                          <label>ตำแหน่ง</label>
                          <input type="text" name="txtPosition" onfocusout="Validate(this.name, this.tagName)" class="form-control txtPosition">
                        </div>
                        
                        <div class="col-md-3 form-group">
                          <label>เบอร์โทร</label>
                          <input type="number" name="txtTel" onfocusout="Validate(this.name, this.tagName)" class="form-control txtTel">
                        </div>

                        <div class="col-md-3 form-group">
                          <label>e-mail</label>
                          <input type="email" name="txtEmail" onfocusout="Validate(this.name, this.tagName)" class="form-control txtEmail">
                        </div>
                      </div>    
                      
                    
                  </div>
                </div>
              </div>
              
              <div class="card">
                <div class="card-header" role="tab">
                  <h6 class="mb-0">
                    <a data-toggle="collapse" href="#div-indicator-choice" aria-expanded="true" aria-controls="div-indicator-choice">
                      <i class="card-icon mdi mdi-checkbox-multiple-marked-circle-outline"></i>
                      <u>ตรวจประเมิน</u>
                    </a>
                  </h6>
                </div>

                <div id="div-indicator-choice" class="collapse show" role="tabpanel" data-parent="#accordion">
                  <div class="card-body">
                    
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="card-footer text-center">
            <button type="" onclick="ValidateClick()" class="btn btn-lg btn-success btnSave">
              <i class="far fa-save"></i> บันทึก
            </button>

            <button type="reset" class="btn btn-lg btn-danger">
              <i class="fa fa-undo"></i> ยกเลิก
            </button>
          </div>
        </div>
      </form>

    </div>
  </div>
  
@endsection

@section('js')
<script type="text/javascript">  

  $(".tbl-choice").on('focusout', ".txtChoice", function () {
    if($(this).closest('.txtChoice').val() == '') {
      $(this).closest('.txtChoice').css('border-color', '#FF0000');
      $('.btnSave').attr('disabled',true);
    } else {
      $(this).closest('.txtChoice').css('border-color', '#2eb82e');
      $('.btnSave').attr('disabled',false)
    }
  });

  function ValidateChoice(name){
    if($('input[name="'+name+'"]').val() == '') {
      $('input[name="'+name+'"]').css('border-color', '#FF0000');
      $('.btnSave').attr('disabled',true);
    }else{
      $('input[name="'+name+'"]').css('border-color', '#2eb82e');
      $('.btnSave').attr('disabled',false)
    }
  }

  function Validate(name, tagname){
    if(tagname == 'SELECT') {
      console.log(tagname);
      if($('select[name="'+name+'"]').val() == 0) {
        $('select[name="'+name+'"]').css('border-color', '#FF0000');
        $('.btnSave').attr('disabled',true);
      }else{
        $('select[name="'+name+'"]').css('border-color', '#2eb82e');
        $('.btnSave').attr('disabled',false)
      }
    } 
    else if(tagname == 'INPUT') {
      console.log(tagname);
      if($('input[name="'+name+'"]').val() == '') {
        $('input[name="'+name+'"]').css('border-color', '#FF0000');
        $('.btnSave').attr('disabled',true);
      }else{
        $('input[name="'+name+'"]').css('border-color', '#2eb82e');
        $('.btnSave').attr('disabled',false)
      }
    }
  }

  function ValidateClick(){ 
    // if($('.txtDateLimit').val()== undefined){
    // alert($('.txtDateLimit').val())
    // }
    // ดักค่าบับันทึกข้อหลัก
    if($('.txtDetail').val()==''){
      $('.btnSave').attr('disabled',true);
      $('.txtDetail').css('border-color','red');
    }
    if($('.ddlRound').val()==1){
        if($('.txtWeight1').val()==''){
          $('.btnSave').attr('disabled',true);
          $('.txtWeight1').css('border-color','red');
        }
    } else {
        if($('.txtWeight21').val()==''){
          $('.btnSave').attr('disabled',true);
          $('.txtWeight21').css('border-color','red');
        }
        if($('.txtWeight22').val()==''){
          $('.btnSave').attr('disabled',true);
          $('.txtWeight22').css('border-color','red');
        }
        if($('.txtWeight23').val()==''){
          $('.btnSave').attr('disabled',true);
          $('.txtWeight23').css('border-color','red');
        }
    }

    if($('.ddlTypeOffice').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlTypeOffice').css('border-color','red');
    }
    if($('.ddlRound').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlRound').css('border-color','red');
    }
    if($('.ddlYear').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlYear').css('border-color','red');
    }

    if($('.ddlTypeAnswer ').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlTypeAnswer ').css('border-color','red');
    }
    if($('.ddlFormula ').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlFormula ').css('border-color','red');
    }
    if($('.ddlScoreGroup').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlScoreGroup').css('border-color','red');
    }

    
  }

  $('.ddlRound').change(function (e) { 
    e.preventDefault();
    let round = $(this).val();
    if(round == 1) {
      $('.txtWeight1').attr({'readonly':false});

      $('.txtWeight21').attr({'readonly':true}).css('border-color','#ccc').val('');
      $('.txtWeight22').attr({'readonly':true}).css('border-color','#ccc').val('');
      $('.txtWeight23').attr({'readonly':true}).css('border-color','#ccc').val('');
    }
    else if(round == 2) { 
      $('.txtWeight21').attr({'readonly':false, 'onfocusout':"Validate(this.name, this.tagName)"});
      $('.txtWeight22').attr({'readonly':false, 'onfocusout':"Validate(this.name, this.tagName)"});
      $('.txtWeight23').attr({'readonly':false, 'onfocusout':"Validate(this.name, this.tagName)"});
      
      $('.txtWeight1').attr({'readonly':true}).css('border-color','#ccc').val('');
    } 
    else {
      $('.txtWeight1').attr({'readonly':true}).css('border-color','#ccc').val('');
      $('.txtWeight21').attr({'readonly':true}).css('border-color','#ccc').val('');
      $('.txtWeight22').attr({'readonly':true}).css('border-color','#ccc').val('');
      $('.txtWeight23').attr({'readonly':true}).css('border-color','#ccc').val('');
    }
    
  });
</script>
@endsection