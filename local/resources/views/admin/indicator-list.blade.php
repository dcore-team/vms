@extends('layouts.master')

@section('title', 'รายงานตัวชี้วัด')
@section('css')
  <style type="">
    
  </style>
@stop
@section('content')
  <div class="row">
    <div class="col-md-8">
      <h2 class="form-inline">ข้อมูลรายงานตัวชี้วัด
        <select class="form-control ml-2 ddlYear font-weight-bold-" onchange="getYear(this.value,$('.ddlRound').val())")">
          <option value="0">เลือกปี พ.ศ.</option>
          @foreach ($ind_year as $y)
            <option value="{{$y->ind_year}}" {{ $y->ind_year == $year ? 'selected':''}}>
              ตัวชี้วัด พ.ศ.{{$y->ind_year}}
            </option>
          @endforeach
        </select>
    
        <select name="" class="form-control ml-2 ddlRound" onchange="getYear($('.ddlYear').val(),this.value)">
          <option value="0">เลือกรอบการประเมิน</option>
          <option value="1" {{$round==1 ?'selected' :''}}>การประเมินรอบที่ 1</option>
          <option value="2" {{$round==2 ?'selected' :''}}>การประเมินรอบที่ 2</option>
        </select>
      </h2>
    </div>

    <div class="col-md-4 text-right">
      <a href="{{url("admin/indicator/create")}}" class="btn btn-lg btn-success">
        <i class="fa fa-plus"></i> เพิ่มข้อมูลตัวชี้วัด
      </a>
    </div>

  </div>

  <div class="row"> 
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <ul class="nav nav-tabs tab-basic" role="tablist">
            @foreach ($office_type_view as $type)
              <li class="nav-item">
                <a class="nav-link {{$type->office_type_id==2 ?'active' :''}}" id="tab-head{{$type->office_type_id}}" data-toggle="tab" href="#tab-type{{$type->office_type_id}}" role="tab" aria-controls="tab-type{{$type->office_type_id}}" aria-selected="{{$type->office_type_id==2 ?'true' :'false'}}">
                  ตัวชี้วัด{{$type->office_type_name}}
                </a>
              </li>
            @endforeach
          </ul>
          
          <div class="tab-content tab-content-basic">
            @foreach ($office_type_view as $type)
              <div class="tab-pane fade {{$type->office_type_id==2 ?'show active' :''}}" id="tab-type{{$type->office_type_id}}" role="tabpanel" aria-labelledby="tab-head{{$type->office_type_id}}">
                <table class="tbl-indicator table-bordered">
                  <thead class="text-center">
                    <tr>
                      <th width="5%">ลำดับ</th>
                      <th width="50%">ตัวชี้วัด</th>
                      <th width="10%">แก้ไข / ลบ</th>
                    </tr>
                  </thead>  
                  <tbody>
                    @php
                      $indicators = DB::table('indicator')
                        ->where([
                          'type_id'=>$type->office_type_id, 
                          'ind_year'=>$year, 'ind_round'=>$round
                        ])
                        ->orderByRaw('indicator.num_01 ASC, LENGTH(indicator.num_02), indicator.num_02 ASC')
                      ->get();
                    @endphp
                    @if(count($indicators) > 0)
                      @foreach ($indicators as $i=>$ind)
                        <tr class="bg-secondary">
                          <td class="text-center">{{$i+1}}</td>
                          <td>
                            @if ($ind->num_02=='#')
                              <span>{{$ind->ind_detail}}</span>
                            @else
                              <span>{{$ind->ind_detail}}</span> 
                              <!-- $ind->ind_id -->
                            @endif
                          </td>
                          <td class="text-center"> 
                            <a href="{{ url('admin/indicator').'/'.$ind->ind_id.'/edit' }}" class="btn btn-icons btn-warning" title="แก้ไข">
                              <i class="fa fa-edit"></i>
                            </a>

                            <button type="button" onclick="DeleterIndicator({{ $ind->ind_id }})" class="btn btn-icons btn-danger">
                              <i class="fa fa-trash-alt"></i>
                            </button>
                          </td>
                        </tr>
                      @endforeach
                    @else
                      <tr>
                        <th colspan="3" class="text-center">ยังไม่มีข้อมูลตัวชี้วัดของปี พ.ศ.{{$year}}</th>
                      </tr>
                    @endif
                  </tbody>
                </table> 
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
<script type="text/javascript"> 
  @if(\Session::has('success'))
    alert('{{ \Session::get('success') }}');
  @endif 
   
  function getYear(year,round) {
    if(year==0 && round==0){ //console.log(1);
      window.location.href='{{url('admin/indicator')}}';
    } else if(year!=0 && round!=0){ //console.log(2);
      // window.location.href='{{url('admin/indicator')}}'+'/'+year+'/'+round;
      window.location.href= '{{url("admin")}}'+'/'+year+'/'+round+'/indicator';
    }
  }

  function EditIndicator(ind_id){ 
    if($('.txtDescFile'+ind_id).val()==''){
      var file = $('.txtDescFileName'+ind_id).val();
    }else{ 
      var file = $('.txtDescFile'+ind_id)[0].files[0];
    }

    var form_data = new FormData();
    form_data.append('_token', '{{csrf_token()}}');
    form_data.append('txtIndID', ind_id);
    form_data.append('txtDetail', $('.txtDetail'+ind_id).val());
    form_data.append('txtDate', $('.txtDate'+ind_id).val());
    form_data.append('txtDescFile', file);
    $.ajax({
      url: '{{ url('indicator/edit') }}',
      type: 'POST',
      data: form_data,
      dataType:'JSON',
      contentType: false,
      processData: false,
      success: function (res) { console.log(res);
        alert(res.msg);
        if($('.txtDescFile'+ind_id).val()!=''){
          $('.desc-downloaded'+ind_id).removeClass('d-none').empty().append(res.download);
        }
        $('.txtDescFileName'+ind_id).val(res.file_name);
        // location.reload();
      }
    });
  }

  function DeleterIndicator(indicator_id) { 
    if (confirm('ต้องการลบข้อมูลหรือไม่ ?')) {
      $.ajax({
        type: "get",
        url: "{{ url('admin/indicator/delete') }}",
        data: {indicator_id: indicator_id},
        success: function (msg) {
          alert(msg);
          location.reload();
        }
      });
    }
  }
</script>
@endsection