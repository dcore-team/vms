@extends('layouts.master')

@section('title', 'ตรวจสอบคะแนน')
@section('css')
  <style>
    
  </style>
@stop
@section('content')
<div class="row">
  <div class="col-md-9">
		<h2 class="form-inline">ตรวจสอบคะแนน
      <select class="form-control ml-2 ddlYear font-weight-bold-" onchange="getYear(this.value,$('.ddlRound').val())")">
        <option value="0">เลือกปี พ.ศ.</option>
        @foreach ($indicator_year_view as $y)
          <option value="{{$y->ind_year}}" {{ $y->ind_year == $year ? 'selected':''}}>
            ตัวชี้วัด พ.ศ.{{$y->ind_year}}
          </option>
        @endforeach
      </select>

      <select name="" class="form-control ml-2 ddlRound" onchange="getYear($('.ddlYear').val(),this.value)">
        <option value="0">เลือกรอบการประเมิน</option>
        @foreach ($round_evaluate_view as $round_evaluate)
          <option value="{{ $round_evaluate->id }}" {{$round==$round_evaluate->id ?'selected' :''}}>{{ $round_evaluate->round_name }}</option>
        @endforeach
      </select>
    </h2>
  </div>

  <div class="col-md-3 text-right">
    {{-- <button type="button" onclick="exportExcel({{$year}},{{$round}})" class="btn btn-lg btn-success" disabled>
      <i class="fas fa-file-excel"></i> Excel
    </button> --}}
  </div>
</div>

<div class="row">
	<div class="col-12">
    <div class="card">
      <div class="card-body">
          <ul class="nav nav-tabs tab-basic" role="tablist">
            @foreach ($office_type_view as $type)
              <li class="nav-item">
                <a class="nav-link {{$type->office_type_id==2 ?'active' :''}}" id="tab-head{{$type->office_type_id}}" data-toggle="tab" href="#tab-type{{$type->office_type_id}}" role="tab" aria-controls="tab-type{{$type->office_type_id}}" aria-selected="{{$type->office_type_id==2 ?'true' :'false'}}">
                  ตัวชี้วัด{{$type->office_type_name}}
                </a>
              </li>
            @endforeach
          </ul>
          
          <div class="tab-content tab-content-basic">
            @foreach ($office_type_view as $type)
              <div class="tab-pane fade {{$type->office_type_id==2 ?'show active' :''}}" id="tab-type{{$type->office_type_id}}" role="tabpanel" aria-labelledby="tab-head{{$type->office_type_id}}">
                <table class="tbl-indicator table-bordered">
                  <thead class="text-center">
                    <tr>
                      <th width="%">หน่วยงาน</th>
                      <th width="%">คะแนนถ่วงน้ำหนักรวม</th>
                    </tr>
                  </thead>  
                  <tbody>
                    
                    @php
                    $total_score = [];
                    if($type->office_type_id!=2){
                        //////////////////////////////////////////  office_type_id != 2 (office) ///////////////////////
                        $weight = '';
                      if($round==1){
                        $weight = 'indicator.ind_weight1';
                      }
                      if($round==2){
                        $weight = 'indicator.ind_weight21';
                      }
                      
                      
                      $total_score = DB::select("SELECT
                        transaction_result.office_id,
                        office.office_name,
                        office_type.office_type_id,
                        office_type.office_type_name,
                        indicator.ind_round,
                        indicator.ind_year,
	                      office.group_weight,
                        SUM( ($weight * transaction_result.score) /100) AS total_score
                        FROM
                          transaction_result
                          INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                            AND transaction_result.round =?
                          INNER JOIN office ON office.office_id = transaction_result.office_id
                          INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                          
                        WHERE
                          indicator.type_id =? 
                          AND indicator.ind_year =?
                          AND indicator.ind_round =?

                        GROUP BY 
                          transaction_result.office_id
                        ORDER BY
                          transaction_result.office_id, indicator.ind_id
                      ", [$round, $type->office_type_id, $year, $round]); 

                    }else{


                        if($round==1){
                            $total_score = DB::select("SELECT
                            transaction_result.office_id,
                            office.office_name,
                            office_type.office_type_name,
                            indicator.ind_round,
                            indicator.ind_year,
                            office.group_weight,
                            SUM( (indicator.ind_weight1 * transaction_result.score) /100)  AS total_score
                            FROM
                              transaction_result
                              INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                                AND transaction_result.round =?
                              INNER JOIN office ON office.office_id = transaction_result.office_id
                              INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                              
                            WHERE
                              indicator.type_id =? 
                              AND indicator.ind_year =?
                              AND indicator.ind_round =?

                            GROUP BY 
                              transaction_result.office_id
                            ORDER BY
                              transaction_result.office_id, indicator.ind_id
                            ", [$round, $type->office_type_id, $year, $round]);
                        }
                        if($round==2){
                          //////////////////////// รอบที่สอง มีสามGroup ////////////////
                        $value_1 = [];
                        $value_2 = [];
                        $value_3 = [];
                        for($i=1;$i<=3;$i++)
                        {
                          if($i==1)
                          {
                              $value_1 = DB::select("SELECT
                              transaction_result.office_id,
                              CONCAT(office.office_name,' [ กลุ่มที่ ',office.group_weight,']') as office_name,
                              office_type.office_type_name,
                              indicator.ind_round,
                              indicator.ind_year,
                              office.group_weight,
                              SUM( (indicator.ind_weight21 * transaction_result.score) /100)  AS total_score
                              FROM
                                transaction_result
                                INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                                  AND transaction_result.round =?
                                INNER JOIN office ON office.office_id = transaction_result.office_id
                                INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                                
                              WHERE
                                indicator.type_id =? 
                                AND indicator.ind_year =?
                                AND indicator.ind_round =?
                                AND office.group_weight = $i
                              GROUP BY 
                                transaction_result.office_id
                              ORDER BY
                                transaction_result.office_id, indicator.ind_id
                              ", [$round, $type->office_type_id, $year, $round]); 
                          }
                          else if($i==2)
                          {
                            $value_2 = DB::select("SELECT
                              transaction_result.office_id,
                              CONCAT(office.office_name,' [ กลุ่มที่ ',office.group_weight,']') as office_name,
                              office_type.office_type_name,
                              indicator.ind_round,
                              indicator.ind_year,
                              office.group_weight,
                              SUM( (indicator.ind_weight22 * transaction_result.score) /100)  AS total_score
                              FROM
                                transaction_result
                                INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                                  AND transaction_result.round =?
                                INNER JOIN office ON office.office_id = transaction_result.office_id
                                INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                                
                              WHERE
                                indicator.type_id =? 
                                AND indicator.ind_year =?
                                AND indicator.ind_round =?
                                AND office.group_weight = $i
                              GROUP BY 
                                transaction_result.office_id
                              ORDER BY
                                transaction_result.office_id, indicator.ind_id
                              ", [$round, $type->office_type_id, $year, $round]);
                          }
                          else if($i==3)
                          {
                            $value_3 = DB::select("SELECT
                              transaction_result.office_id,
                              CONCAT(office.office_name,' [ กลุ่มที่ ',office.group_weight,']') as office_name,
                              office_type.office_type_name,
                              indicator.ind_round,
                              indicator.ind_year,
                              office.group_weight,
                              SUM( (indicator.ind_weight23 * transaction_result.score) /100)  AS total_score
                              FROM
                                transaction_result
                                INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                                  AND transaction_result.round =?
                                INNER JOIN office ON office.office_id = transaction_result.office_id
                                INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                                
                              WHERE
                                indicator.type_id =? 
                                AND indicator.ind_year =?
                                AND indicator.ind_round =?
                                AND office.group_weight = $i
                              GROUP BY 
                                transaction_result.office_id
                              ORDER BY
                                transaction_result.office_id, indicator.ind_id
                              ", [$round, $type->office_type_id, $year, $round]);
                          }
                        }

                        // echo "----".count($value_1)."<br>";
                        if(count($value_1) > 0)
                          foreach ($value_1 as $key => $total) {
                            array_push($total_score,$value_1[$key]);
                          }

                          if(count($value_2) > 0)
                            foreach ($value_2 as $key => $total) {
                              array_push($total_score,$value_2[$key]);
                            }
                        // echo "----".count($value_3)."<br>";
                        if(count($value_3) > 0)
                          foreach ($value_3 as $key => $total) {
                            array_push($total_score,$value_3[$key]);
                            // echo $total->office_name."  ";
                            //   echo number_format($total->total_score,4,'.','')."<br>";
                          } 
                      }
                    }
                    /////////////////////////////////////////////end office_type_id != 2 (office)////////////////////////////////////////

                    @endphp

                    @if(count($total_score) > 0)
                      @foreach ($total_score as $i=>$total)
                        <tr>
                          <td>
                            {{ $total->office_name }}
                          </td>
                          <td>
                            {{ number_format($total->total_score,4,'.','') }}
                          </td>
                        </tr>

                        @php
                          // if($total->ind_round==2 && $total->office_type_id!=2){ //echo '52';
                          //   $weight = 'indicator.ind_weight21';
                          // }

                          // if($total->ind_round==2 && $total->group_weight==1){ //echo '22 1';
                          //   $weight = 'indicator.ind_weight21';
                          // }

                          // if($total->ind_round==2 && $total->group_weight==2){ //echo '22 2';
                          //   $weight = 'indicator.ind_weight22';
                          // }

                          // if($total->ind_round==2 && $total->group_weight==3){  //echo '22 3';
                          //   $weight = 'indicator.ind_weight23';
                          // }
                        @endphp
                      @endforeach
                    @else
                      <tr>
                        <th colspan="2" class="text-center">ยังไม่มีข้อมูลของปี พ.ศ.{{$year}} รอบการประเมินที่ {{$round}}</th>
                      </tr>
                    @endif


                  </tbody>
                </table> 
              </div>
            @endforeach
          </div>
        </div>

  

    </div>
	</div>
</div>
@endsection

@section('js')
<script>

  
  function getYear(year,round) {
    if(year==0 && round==0){ //console.log(1);
      window.location.href='{{url('admin/checkscore')}}';
    } else if(year!=0 && round!=0){ //console.log(2);
      window.location.href= '{{url("admin")}}'+'/'+year+'/'+round+'/checkscore';
    }
  }
</script>
@endsection
