<html>
<head>
  <title>
    ตารางสรุปคะแนนผลการประเมิน รอบการประเมินที่ {{ $round }} พ.ศ.{{ $year }}
  </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{ asset('bootstrap-3.3.7/css/bootstrap.min.css') }}">
  <style>
    @font-face {
      font-family: 'THSarabun';
      font-style: normal;
      font-weight: normal;
      src: url('{{ asset('fonts/THSarabun.ttf') }}') format('truetype');
    }
    @font-face {
      font-family: 'THSarabun';
      font-style: normal;
      font-weight: bold;
      src: url('{{ asset('fonts/THSarabun Bold.ttf') }}') format('truetype');
    }

    @font-face {
      font-family: 'THSarabunNew';
      font-style: normal;
      font-weight: normal;
      src: url('{{ asset('fonts/THSarabunNew.ttf') }}') format('truetype');
    }
    @font-face {
      font-family: 'THSarabun';
      font-style: normal;
      font-weight: bold;
      src: url('{{ asset('fonts/THSarabunNew Bold.ttf') }}') format('truetype');
    }
  </style>

  <style>
    @page {
      /*t1.5 r b l 2.54*/
      /* padding: 1.5cm 1cm 1.5cm 1cm;  */
      /* padding-top: 1.5cm;
      padding-right: 1cm;
      padding-left: 1cm;
      padding-top: 1.5cm; */
      /* padding: 20px; */
    } 
/* body { margin: 0px; } */

    body,h1,h2,h3,h4,h5, .page-num{
      font-family: 'THSarabun';
      color: black;
    }
    header,h1,h2,h3,h4,h5, .page-num{
      font-weight: bold;
    }

    header {
      position: fixed;
      top: -1cm;
      left: 0cm;
      right: 0cm;
      height: 3cm;
      /* border:solid red 1px; */
    }

    main {
      position: relative;
      top: 2cm;
      /* left: 0cm;
      right: 0cm; */
      /* border: solid red 1px; */
    }

    footer {
      position: fixed; 
      bottom: -1cm;
      left: 0cm;
      right: 0cm;
      height: 1cm;
      text-align: center;
      /* border: solid red 1px; */
    }


    table {
      /* width: 640px; */
      /* table-layout: fixed; */
      width: 100%;
    }

    .tbl-score th, .tbl-score td{ 
      border: 1px solid black;
      /* word-wrap: break-word; */
      white-space: wrap;
    }

    .tbl-score th{ 
      /*t r b l*/
      /* padding-top:-5px
      padding-left: 5px;
      padding-right: 5px; */
      padding: 0px 5px 5px;
      font-weight: bold;
    }
    .tbl-score td{ 
      padding-top:-5px;
      padding-bottom: 5px;
      padding-left: 5px;
      padding-right: 5px;
      font-weight: normal;
    }

    .page-num:after{
      counter-increment: pages;
      content: counter(pages); 
      /* ' of ' counter(pages); */
    }

    .font-14{
      font-size: 14px !important;
    }
    .font-16{
      font-size: 16px !important;
    }
    .font-18{
      font-size: 18px !important;
    }
    .font-20{
      font-size: 20px !important;
    }  
  </style>
</head>
<body>
  <header>
    <div class="row text-right vertical-middle" style="padding-top: px;">
      <span class="page-num align-middle" style="font-size:16px;"></span>
    </div>
    
    <div class="row">
      <div class="col-12 text-center font-weight-bold">
        <h3 style="padding-top:-40px;">
          ตารางสรุปคะแนนผลการประเมินการปฎิบัติราชการตามคำรับรองการปฎิบัติราชการ
          {{-- <br> 
          รอบการประเมินที่ {{ $round }} พ.ศ.{{ $year }} --}}
        </h3>
        <h3 style="padding-top:-30px;">
          รอบการประเมินที่ {{ $round }} พ.ศ.{{ $year }}
        </h3>

        <div class="font-20" style="padding-top:-20px;">
          {{ $inspector->position }} @if($inspector->name!=null) ({{ $inspector->name }}) @endif
        </div>
      </div>
    </div>
  </header>

  <footer>
    {{-- Copyright &copy; {{ date("Y") }}  --}}
  </footer>

  <main style="width:100%">
      <table class="tbl-score font-14" style="positon:relative;width:100%">
        <thead class="">
          <tr>
            <th class="text-center" width="%">ลำดับ</th>
            <th class="text-center" width="%">หน่วยงาน</th>
            <th class="text-center" width="%">คะแนนถ่วงน้ำหนักรวม</th>
          </tr>
        </thead>

        <tbody>
          @php 
            $total_score = [];
            $sum_score = 0;
            $avg_score = 0;

            if($round==1) {
              $total_score = DB::select("SELECT
                transaction_result.office_id,
                office.office_name,
                office_type.office_type_name,
                indicator.ind_round,
                indicator.ind_year,
                office.group_weight,
                SUM( (indicator.ind_weight1 * transaction_result.score) /100)  AS total_score
                FROM
                  transaction_result
                  INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                    AND transaction_result.round =?
                  INNER JOIN office ON office.office_id = transaction_result.office_id
                  INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                  
                WHERE indicator.ind_year =?
                  AND indicator.ind_round =?
                  AND office.inspector_zone = ?

                GROUP BY 
                  transaction_result.office_id
                ORDER BY
                  total_score DESC
              ", [$round, $year, $round, $inspector_zone]); 
            }

            if($round==2) {
              //////////////////////// รอบที่สอง มีสามGroup ////////////////
              $value_1 = [];
              $value_2 = [];
              $value_3 = [];

              for($i=1;$i<=3;$i++) {
                if($i==1) {
                  $value_1 = DB::select("SELECT
                    transaction_result.office_id,
                    --CONCAT(office.office_name,' [ กลุ่มที่ ',office.group_weight,']') as office_name,
                    office.office_name,
                    office_type.office_type_name,
                    indicator.ind_round,
                    indicator.ind_year,
                    office.group_weight,
                    SUM( (indicator.ind_weight21 * transaction_result.score) /100)  AS total_score
                    FROM
                      transaction_result
                      INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                        AND transaction_result.round =?
                      INNER JOIN office ON office.office_id = transaction_result.office_id
                      INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                      
                    WHERE indicator.ind_year =?
                      AND indicator.ind_round =?
                      AND office.inspector_zone =?
                      AND office.group_weight = $i
                    GROUP BY 
                      transaction_result.office_id
                    ORDER BY
                      total_score DESC
                  ", [$round, $year, $round, $inspector_zone]); 
                }

                else if($i==2) {
                  $value_2 = DB::select("SELECT
                    transaction_result.office_id,
                    office.office_name,
                    office_type.office_type_name,
                    indicator.ind_round,
                    indicator.ind_year,
                    office.group_weight,
                    SUM( (indicator.ind_weight22 * transaction_result.score) /100)  AS total_score
                    FROM
                      transaction_result
                      INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                        AND transaction_result.round =?
                      INNER JOIN office ON office.office_id = transaction_result.office_id
                      INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                      
                    WHERE indicator.ind_year =?
                      AND indicator.ind_round =?
                      AND office.inspector_zone = ?
                      AND office.group_weight = $i
                    GROUP BY 
                      transaction_result.office_id
                    ORDER BY
                      total_score DESC
                  ", [$round, $year, $round, $inspector_zone]); 
                }

                else if($i==3) {
                  $value_3 = DB::select("SELECT
                    transaction_result.office_id,
                    office.office_name,
                    office_type.office_type_name,
                    indicator.ind_round,
                    indicator.ind_year,
                    office.group_weight,
                    SUM( (indicator.ind_weight23 * transaction_result.score) /100)  AS total_score
                    FROM
                      transaction_result
                      INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                        AND transaction_result.round =?
                      INNER JOIN office ON office.office_id = transaction_result.office_id
                      INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                      
                      WHERE indicator.ind_year =?
                      AND indicator.ind_round =?
                      AND office.inspector_zone = ?
                      AND office.group_weight = $i
                    GROUP BY 
                      transaction_result.office_id
                    ORDER BY
                      total_score DESC
                  ", [$round, $year, $round, $inspector_zone]); 
                }
              }

              // echo "----".count($value_1)."<br>";
              if(count($value_1) > 0)
                foreach ($value_1 as $key => $total) {
                  array_push($total_score,$value_1[$key]);
                }

                if(count($value_2) > 0)
                  foreach ($value_2 as $key => $total) {
                    array_push($total_score,$value_2[$key]);
                  }
              // echo "----".count($value_3)."<br>";
              if(count($value_3) > 0)
                foreach ($value_3 as $key => $total) {
                  array_push($total_score,$value_3[$key]);
                  // echo $total->office_name."  ";
                  //   echo number_format($total->total_score,4,'.','')."<br>";
                } 
            }
          @endphp

          @if(count($total_score) > 0)
            @foreach ($total_score as $i=>$total)
              @php
                $sum_score += $total->total_score;
              @endphp

              <tr>
                <td class="text-center"> {{ ($i+1) }} </td>

                <td>
                  {{ $total->office_name }}
                </td>
                
                <td>
                  {{ number_format($total->total_score,4,'.','') }}
                </td>
              </tr>
            @endforeach
            <tr class="bg-secondary">
              <th colspan="2" class="text-center">รวม</th>

              <th>{{ number_format( ($sum_score / count($total_score)) ,4,'.','') }}</th>
            </tr>
          @else
            <tr>
              <th colspan="3" class="text-center">ยังไม่มีข้อมูลของปี พ.ศ.{{$year}} รอบการประเมินที่ {{$round}}</th>
            </tr>
          @endif  
        </tbody>
        
      </table>
  </main>
</body>
</html>