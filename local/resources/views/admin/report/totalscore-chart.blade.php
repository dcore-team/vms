@extends('layouts.master')

@section('title', 'ผลคะแนนของหน่วยงาน (Chart)')
@section('css')
  <style>
    
  </style>
@stop
@section('content')
<div class="row">
  <div class="col-md-9">
		<h2 class="form-inline">
      รายงานสรุปผลคะแนนของหน่วยงาน (Chart)
    </h2>
  </div>

  <div class="col-md-3 text-right">
    {{-- <button type="button" onclick="print({{$year}},{{$round}})" class="btn btn-lg btn-primary">
      <i class="fa fa-print"></i> พิมพ์
    </button> --}}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <h2 class="form-inline">
      <select class="form-control ml-2 ddlYear font-weight-bold-" onchange="getYear(this.value,$('.ddlRound').val(),$('.ddlInspector').val())">
        <option value="0">เลือกปี พ.ศ.</option>
        @foreach ($indicator_year_view as $y)
          <option value="{{$y->ind_year}}" {{ $y->ind_year == $year ? 'selected':''}}>
            ตัวชี้วัด พ.ศ.{{$y->ind_year}}
          </option>
        @endforeach
      </select>

      <select name="" class="form-control ml-2 ddlRound" onchange="getYear($('.ddlYear').val(),this.value,$('.ddlInspector').val())">
        <option value="0">เลือกรอบการประเมิน</option>
        @foreach ($round_evaluate_view as $round_evaluate)
          <option value="{{ $round_evaluate->id }}" {{$round==$round_evaluate->id ?'selected' :''}}>{{ $round_evaluate->round_name }}</option>
        @endforeach
      </select>
    </h2>
  </div>
</div>

<div class="row">
  {{-- <ul class="nav nav-tabs tab-basic" role="tablist">
    @foreach ($office_type as $type)
      <li class="nav-item">
        <a class="nav-link {{$type->office_type_id==2 ?'active' :''}}" id="tab-head{{$type->office_type_id}}" data-toggle="tab" href="#tab-type{{$type->office_type_id}}" role="tab" aria-controls="tab-type{{$type->office_type_id}}" aria-selected="{{$type->office_type_id==2 ?'true' :'false'}}">
          ตัวชี้วัด{{$type->office_type_name}}
        </a>
      </li>
    @endforeach
  </ul>
  
  <div class="tab-content tab-content-basic">
    @foreach ($office_type as $type)
      <div class="tab-pane fade {{$type->office_type_id==2 ?'show active' :''}}" id="tab-type{{$type->office_type_id}}" role="tabpanel" aria-labelledby="tab-head{{$type->office_type_id}}">
        <table class="tbl-indicator table-bordered">
          <thead class="text-center">
            <tr>
              <th width="%">หน่วยงาน</th>
              <th width="%">คะแนนถ่วงน้ำหนักรวม</th>
            </tr>
          </thead>  
          <tbody>
            
            @php
            $total_score = [];
            if($type->office_type_id!=2){
                //////////////////////////////////////////  office_type_id != 2 (office) ///////////////////////
                $weight = '';
              if($round==1){
                $weight = 'indicator.ind_weight1';
              }
              if($round==2){
                $weight = 'indicator.ind_weight21';
              }
              
              
              $total_score = DB::select("SELECT
                transaction_result.office_id,
                office.office_name,
                office_type.office_type_id,
                office_type.office_type_name,
                indicator.ind_round,
                indicator.ind_year,
                office.group_weight,
                SUM( ($weight * transaction_result.score) /100) AS total_score
                FROM
                  transaction_result
                  INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                    AND transaction_result.round =?
                  INNER JOIN office ON office.office_id = transaction_result.office_id
                  INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                  
                WHERE
                  indicator.type_id =? 
                  AND indicator.ind_year =?
                  AND indicator.ind_round =?

                GROUP BY 
                  transaction_result.office_id
                ORDER BY
                  total_score DESC
              ", [$round, $type->office_type_id, $year, $round]); 

            }else{
                if($round==1){
                    $total_score = DB::select("SELECT
                    transaction_result.office_id,
                    office.office_name,
                    office_type.office_type_name,
                    indicator.ind_round,
                    indicator.ind_year,
                    office.group_weight,
                    SUM( (indicator.ind_weight1 * transaction_result.score) /100)  AS total_score
                    FROM
                      transaction_result
                      INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                        AND transaction_result.round =?
                      INNER JOIN office ON office.office_id = transaction_result.office_id
                      INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                      
                    WHERE
                      indicator.type_id =? 
                      AND indicator.ind_year =?
                      AND indicator.ind_round =?

                    GROUP BY 
                      transaction_result.office_id
                    ORDER BY
                      total_score DESC
                    ", [$round, $type->office_type_id, $year, $round]);
                }
                if($round==2){
                  //////////////////////// รอบที่สอง มีสามGroup ////////////////
                $value_1 = [];
                $value_2 = [];
                $value_3 = [];
                for($i=1;$i<=3;$i++)
                {
                  if($i==1)
                  {
                      $value_1 = DB::select("SELECT
                      transaction_result.office_id,
                      CONCAT(office.office_name,' [ กลุ่มที่ ',office.group_weight,']') as office_name,
                      office_type.office_type_name,
                      indicator.ind_round,
                      indicator.ind_year,
                      office.group_weight,
                      SUM( (indicator.ind_weight21 * transaction_result.score) /100)  AS total_score
                      FROM
                        transaction_result
                        INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                          AND transaction_result.round =?
                        INNER JOIN office ON office.office_id = transaction_result.office_id
                        INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                        
                      WHERE
                        indicator.type_id =? 
                        AND indicator.ind_year =?
                        AND indicator.ind_round =?
                        AND office.group_weight = $i
                      GROUP BY 
                        transaction_result.office_id
                      ORDER BY
                        total_score DESC
                      ", [$round, $type->office_type_id, $year, $round]); 
                  }
                  else if($i==2)
                  {
                    $value_2 = DB::select("SELECT
                      transaction_result.office_id,
                      CONCAT(office.office_name,' [ กลุ่มที่ ',office.group_weight,']') as office_name,
                      office_type.office_type_name,
                      indicator.ind_round,
                      indicator.ind_year,
                      office.group_weight,
                      SUM( (indicator.ind_weight22 * transaction_result.score) /100)  AS total_score
                      FROM
                        transaction_result
                        INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                          AND transaction_result.round =?
                        INNER JOIN office ON office.office_id = transaction_result.office_id
                        INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                        
                      WHERE
                        indicator.type_id =? 
                        AND indicator.ind_year =?
                        AND indicator.ind_round =?
                        AND office.group_weight = $i
                      GROUP BY 
                        transaction_result.office_id
                      ORDER BY
                        total_score DESC
                      ", [$round, $type->office_type_id, $year, $round]);
                  }
                  else if($i==3)
                  {
                    $value_3 = DB::select("SELECT
                      transaction_result.office_id,
                      CONCAT(office.office_name,' [ กลุ่มที่ ',office.group_weight,']') as office_name,
                      office_type.office_type_name,
                      indicator.ind_round,
                      indicator.ind_year,
                      office.group_weight,
                      SUM( (indicator.ind_weight23 * transaction_result.score) /100)  AS total_score
                      FROM
                        transaction_result
                        INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                          AND transaction_result.round =?
                        INNER JOIN office ON office.office_id = transaction_result.office_id
                        INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                        
                      WHERE
                        indicator.type_id =? 
                        AND indicator.ind_year =?
                        AND indicator.ind_round =?
                        AND office.group_weight = $i
                      GROUP BY 
                        transaction_result.office_id
                      ORDER BY
                        total_score DESC
                      ", [$round, $type->office_type_id, $year, $round]);
                  }
                }

                // echo "----".count($value_1)."<br>";
                if(count($value_1) > 0)
                  foreach ($value_1 as $key => $total) {
                    array_push($total_score,$value_1[$key]);
                  }

                  if(count($value_2) > 0)
                    foreach ($value_2 as $key => $total) {
                      array_push($total_score,$value_2[$key]);
                    }
                // echo "----".count($value_3)."<br>";
                if(count($value_3) > 0)
                  foreach ($value_3 as $key => $total) {
                    array_push($total_score,$value_3[$key]);
                    // echo $total->office_name."  ";
                    //   echo number_format($total->total_score,4,'.','')."<br>";
                  } 
              }
            }
            /////////////////////////////////////////////end office_type_id != 2 (office)////////////////////////////////////////

            @endphp

            @if(count($total_score) > 0)
              @foreach ($total_score as $i=>$total)
                <tr class="bg-secondary">
                  <td>
                    {{ $total->office_name }}
                  </td>
                  <td>
                    {{ number_format($total->total_score,4,'.','') }}
                  </td>
                </tr>

                @php
                  // if($total->ind_round==2 && $total->office_type_id!=2){ //echo '52';
                  //   $weight = 'indicator.ind_weight21';
                  // }

                  // if($total->ind_round==2 && $total->group_weight==1){ //echo '22 1';
                  //   $weight = 'indicator.ind_weight21';
                  // }

                  // if($total->ind_round==2 && $total->group_weight==2){ //echo '22 2';
                  //   $weight = 'indicator.ind_weight22';
                  // }

                  // if($total->ind_round==2 && $total->group_weight==3){  //echo '22 3';
                  //   $weight = 'indicator.ind_weight23';
                  // }
                @endphp
              @endforeach
            @else
              <tr>
                <th colspan="2" class="text-center">ยังไม่มีข้อมูลของปี พ.ศ.{{$year}} รอบการประเมินที่ {{$round}}</th>
              </tr>
            @endif


          </tbody>
        </table> 
      </div>
    @endforeach
  </div> --}}
  @php
    // $arr_office21 = json_encode($arr_office2);
    // echo $arr_office2;
    // echo '<pre>';
    //   print_r($arr_office21);
    // echo '</pre>';
     $arr_office2 = [];
      $arr_score_weight2 = [];

      $arr_office3 = [];
      $arr_score_weight3 = [];

      $arr_office4 = [];
      $arr_score_weight4 = [];

      $arr_office5 = [];
      $arr_score_weight5 = [];
  @endphp

  @foreach ($office_type_view as $k=>$type)
    @php
       if($round==1){
          $total_score = DB::select("SELECT
            transaction_result.office_id,
            office.short_name,
            office_type.office_type_name,
            -- indicator.ind_round,
            -- indicator.ind_year,
            -- office.group_weight,
            SUM( (indicator.ind_weight1 * transaction_result.score) /100)  AS total_score
            FROM
              transaction_result
              INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                AND transaction_result.round =?
              INNER JOIN office ON office.office_id = transaction_result.office_id
              INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
              
            WHERE indicator.ind_year =?
              AND indicator.ind_round =?
              AND indicator.type_id =?
            GROUP BY 
              transaction_result.office_id
            ORDER BY
              total_score DESC
              -- transaction_result.office_id ASC
          ", [$round, $year, $round, $type->office_type_id]);

          switch ($type->office_type_id) {
            case '2':
              if(count($total_score) > 0){
                foreach ($total_score as $key => $total) {
                  // array_push($arr_score_weight2,$value_1[$key]);
                  array_push($arr_office2,$total->short_name);
                  array_push($arr_score_weight2,$total->total_score);
                }
              }
            break;
  
            case '3':
              if(count($total_score) > 0){
                foreach ($total_score as $key=>$total) {
                  array_push($arr_office3,$total->short_name);
                  array_push($arr_score_weight3,$total->total_score);
                }
              }
            break;
  
            case '4':
              if(count($total_score) > 0){
                foreach ($total_score as $key=>$total) {
                  array_push($arr_office4,$total->short_name);
                  array_push($arr_score_weight4,$total->total_score);
                }
              }
            break;
  
            case '5':
              if(count($total_score) > 0){
                foreach ($total_score as $key=>$total) {
                  array_push($arr_office5,$total->short_name);
                  array_push($arr_score_weight5,$total->total_score);
                }
              }
            break;
          }
        }

        if($round==2) {
          $group_1 = [];
          $group_2 = [];
          $group_3 = [];

          for($i=1; $i<=3; $i++) {
            if($i==1) {
              $group_1 = DB::select("SELECT
                transaction_result.office_id,
                office.short_name,
                office_type.office_type_name,
                SUM( (indicator.ind_weight21 * transaction_result.score) /100)  AS total_score
                FROM
                  transaction_result
                  INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                    AND transaction_result.round =?
                  INNER JOIN office ON office.office_id = transaction_result.office_id
                  INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                  
                WHERE indicator.ind_year =?
                  AND indicator.ind_round =?
                  AND indicator.type_id =?
                  AND office.group_weight = $i
                GROUP BY 
                  transaction_result.office_id
                ORDER BY
                  total_score DESC
                  -- transaction_result.office_id ASC
              ", [$round, $year, $round, $type->office_type_id]); 
            }

            else if($i==2) {
              $group_2 = DB::select("SELECT
                transaction_result.office_id,
                office.short_name,
                office_type.office_type_name,
                SUM( (indicator.ind_weight22 * transaction_result.score) /100)  AS total_score
                FROM
                  transaction_result
                  INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                    AND transaction_result.round =?
                  INNER JOIN office ON office.office_id = transaction_result.office_id
                  INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                  
                WHERE indicator.ind_year =?
                  AND indicator.ind_round =?
                  AND indicator.type_id =?
                  AND office.group_weight = $i
                GROUP BY 
                  transaction_result.office_id
                ORDER BY
                  total_score DESC
              ", [$round, $year, $round, $type->office_type_id]); 
            }

            else if($i==3) {
              $group_3 = DB::select("SELECT
                transaction_result.office_id,
                office.short_name,
                office_type.office_type_name,
                SUM( (indicator.ind_weight23 * transaction_result.score) /100)  AS total_score
                FROM
                  transaction_result
                  INNER JOIN indicator ON indicator.ind_id = transaction_result.ind_id 
                    AND transaction_result.round =?
                  INNER JOIN office ON office.office_id = transaction_result.office_id
                  INNER JOIN office_type ON office_type.office_type_id = office.office_type_id  
                  
                  WHERE indicator.ind_year =?
                  AND indicator.ind_round =?
                  AND indicator.type_id =?
                  AND office.group_weight = $i
                GROUP BY 
                  transaction_result.office_id
                ORDER BY
                  total_score DESC
              ", [$round, $year, $round, $type->office_type_id]); 
            }

          }
          
          switch ($type->office_type_id) {
            case '2':
              if(count($group_1) > 0){
                foreach ($group_1 as $key => $total) {
                  array_push($arr_office2,$total->short_name);
                  array_push($arr_score_weight2,$total->total_score);
                }
              }
  
              if(count($group_2) > 0){
                foreach ($group_2 as $key => $total) {
                  array_push($arr_office2,$total->short_name);
                  array_push($arr_score_weight2,$total->total_score);
                }
              }
              
              if(count($group_3) > 0){
                foreach ($group_3 as $key => $total) {
                  array_push($arr_office2,$total->short_name);
                  array_push($arr_score_weight2,$total->total_score);
                } 
              }
            break;
  
            case '3':
              if(count($group_1) > 0){
                foreach ($group_1 as $key=>$total) {
                  array_push($arr_office3,$total->short_name);
                  array_push($arr_score_weight3,$total->total_score);
                }
              }
            break;
  
            case '4':
              if(count($group_1) > 0){
                foreach ($group_1 as $key=>$total) {
                  array_push($arr_office4,$total->short_name);
                  array_push($arr_score_weight4,$total->total_score);
                }
              }
            break;
  
            case '5':
              if(count($group_1) > 0){
                foreach ($group_1 as $key=>$total) {
                  array_push($arr_office5,$total->short_name);
                  array_push($arr_score_weight5,$total->total_score);
                }
              }
            break;
          }
            
        }
    @endphp
      
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">{{ $type->office_type_name }}</h4>
          <canvas id="barChart{{ ($k+2) }}" style="height:230px"></canvas>
        </div>
      </div>
    </div>
  @endforeach

  @php
    $arr_office2 = json_encode($arr_office2);
    $arr_score_weight2 = json_encode($arr_score_weight2);
    $arr_office3 = json_encode($arr_office3);
    $arr_score_weight3 = json_encode($arr_score_weight3);
    $arr_office4 = json_encode($arr_office4);
    $arr_score_weight4 = json_encode($arr_score_weight4);
    $arr_office5 = json_encode($arr_office5);
    $arr_score_weight5 = json_encode($arr_score_weight5);
  @endphp
  
  
</div>
@endsection

@section('js')
<script>

  
  function getYear(year,round) {
    if(year==0 && round==0){ //console.log(1);
      window.location.href='{{url('admin/scorechart')}}';
    } else if(year!=0 && round!=0){ //console.log(2);
      window.location.href= '{{url("admin")}}'+'/'+year+'/'+round+'/scorechart';
    }
  }


  function getRandomColorHex() {
    // var hex = "0123456789ABCDEF", color = "#";
    // for (var i = 1; i <= 6; i++) {
    //   color += hex[Math.floor(Math.random() * 16)];
    // }
    // return color;
    // var num = Math.round(0xffffff * Math.random());
    // var r = num >> 16;
    // var g = num >> 8 & 255;
    // var b = num & 255;
    var r = Math.floor(Math.random()*256); 
    var g = Math.floor(Math.random()*256);       
    var b = Math.floor(Math.random()*256); 
    return 'rgba('+r+', '+g+', '+b+', 0.4' +')';
  }
    
  //ศฝ
  var arr_office2 = {!! $arr_office2 !!}; 
  // alert(arr_office2.length)
  var bgcolor2 = [];
  for (let i=0; i<arr_office2.length; i++) {
    bgcolor2.push(getRandomColorHex());
  } console.log(bgcolor2);
  
  var arr_score_weight2 = {!! $arr_score_weight2 !!};
  var data2 = {
    labels: arr_office2,
    datasets: [{
      label: 'คะแนนที่ได้ ',
      data: arr_score_weight2,
      backgroundColor: bgcolor2,
      // [
      //   'rgba(255, 99, 132, 0.2)',
      //   'rgba(54, 162, 235, 0.2)',
      //   'rgba(255, 206, 86, 0.2)',
      //   'rgba(75, 192, 192, 0.2)',
      //   'rgba(153, 102, 255, 0.2)',
      //   'rgba(255, 159, 64, 0.2)'
      // ],
      borderColor: [
        // 'rgba(255,99,132,1)',
        // 'rgba(54, 162, 235, 1)',
        // 'rgba(255, 206, 86, 1)',
        // 'rgba(75, 192, 192, 1)',
        // 'rgba(153, 102, 255, 1)',
        // 'rgba(255, 159, 64, 1)'
        '#979797',
      ],
      borderWidth: 1
    }]
  };

  var arr_office3 = {!! $arr_office3 !!}; 
  var bgcolor3 = [];
  for (let i=0; i<arr_office3.length; i++) {
    bgcolor3.push(getRandomColorHex());
  }
  var arr_score_weight3 = {!! $arr_score_weight3 !!};
  var data3 = {
    labels: arr_office3, 
    datasets: [{
      label: 'คะแนนที่ได้ ',
      data: arr_score_weight3 ,
      backgroundColor: bgcolor3,
      borderColor: [
        '#979797',
      ],
      borderWidth: 1
    }]
  };

  var arr_office4 = {!! $arr_office4 !!}; 
  var bgcolor4 = [];
  for (let i=0; i<arr_office4.length; i++) {
    bgcolor4.push(getRandomColorHex());
  }
  var arr_score_weight4 = {!! $arr_score_weight4 !!};
  var data4 = {
    labels: arr_office4, 
    datasets: [{
      label: 'คะแนนที่ได้ ',
      data: arr_score_weight4 ,
      backgroundColor: bgcolor4,
      borderColor: [
        '#979797',
      ],
      borderWidth: 1
    }]
  };

  var arr_office5 = {!! $arr_office5 !!}; 
  var bgcolor5 = [];
  for (let i=0; i<arr_office5.length; i++) {
    bgcolor5.push(getRandomColorHex());
  }
  var arr_score_weight5 = {!! $arr_score_weight5 !!};
  var data5 = {
    labels: arr_office5, 
    datasets: [{
      label: 'คะแนนที่ได้ ',
      data: arr_score_weight5 ,
      backgroundColor: bgcolor5,
      borderColor: [
        '#979797',
      ],
      borderWidth: 1
    }]
  };

  var options = {
    // maintainAspectRatio: false, //ยาว
    decimals: 4,
    scales: {
      yAxes: [{
        barPercentage: 1,
        ticks: {
          beginAtZero: true,
          // steps: 1,
          // stepValue: ,
          stepSize: 1,
          min: 0,
          max: 5
        },
        gridLines: {
          // display: false
          zeroLineWidth: 1
        }
      }],
      xAxes: [{
        barPercentage: 0.5, //width bar
        scaleLabel: {
          display: true,
          labelString: ''
        },
        ticks: {
          // beginAtZero: true,
          // display: false, //this will remove only the label
          // maxRotation: 90,
          minRotation: 90 //sey vertical label X
        },
        gridLines: {
          // zeroLineColor: "black",
          zeroLineWidth: 1
          // display: false
        }
      }]
    },
    legend: {
      display: false
    },
    elements: {
      point: {
        radius: 5
      }
    }

  };

  if ($("#barChart2").length) {
    var barChartCanvas = $("#barChart2").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: data2,
      options: options
    });
  }

  if ($("#barChart3").length) { 
    var barChartCanvas = $("#barChart3").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: data3,
      options: options
    });
  }
    
  if ($("#barChart4").length) { 
    var barChartCanvas = $("#barChart4").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: data4,
      options: options
    });
  }

  if ($("#barChart5").length) { 
    var barChartCanvas = $("#barChart5").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: data5,
      options: options
    });
  }
</script>
@endsection
