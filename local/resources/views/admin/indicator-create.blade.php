@extends('layouts.master')

@section('title', 'เพิ่มข้อมูลตัวชี้วัด')
@section('css')
  <style media="screen">
    .btnDeleteChoice{
      cursor: pointer;
    }
  </style>
@stop
@section('content')
  <div class="row">
    <div class="col-md-12">
      <h2 class="form-inline">เพิ่มข้อมูลตัวชี้วัด
      </h1>
    </div>

  </div>

  <div class="row"> 
    <div class="col-md-12">

      <form id="frmInsert" action="{{ url('admin/indicator/create') }}" method="POST" enctype="multipart/form-data"> 
      {{-- <form id="frmInsert" method="post"> --}}
        @csrf
        <div class="card">
          <div class="card-body pb-3">

            <div class="accordion basic-accordion" id="accordion" role="tablist">
              <div class="card">
                <div class="card-header" role="tab">
                  <h6 class="mb-0">
                    <a data-toggle="collapse" href="#div-indicator" aria-expanded="true" aria-controls="div-indicator">
                      {{-- <i class="card-icon fa fa-file-invoice"></i> --}}
                      <i class="card-icon mdi mdi-checkbox-marked-circle-outline"></i><u>ข้อมูลตัวชี้วัด</u>
                    </a>
                  </h6>
                </div>

                <div id="div-indicator" class="collapse show" role="tabpanel" data-parent="#accordion">
                  <div class="card-body">
                    
                      <div class="row">
                        <div class="col-md-4 form-group">
                          <label>ประเภทหน่วยงาน</label>
                          <select name="ddlTypeOffice" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlTypeOffice">
                            <option value="0">เลือกประเภทหน่วยงาน</option>
                            @foreach ($office_type_view as $type)
                              <option value="{{ $type->office_type_id }}" >
                                ตัวชี้วัด {{ $type->office_type_name }}
                              </option>
                            @endforeach
                          </select>
                        </div>
          
                        <div class="col-md-4 form-group">
                          <label>ปี พ.ศ.</label>
                          <select name="ddlYear" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlYear" >
                            <option value="0">เลือกปี พ.ศ.</option>
                            {{-- <option value="{{ (date('Y')+543) }}" selected>
                              ตัวชี้วัด พ.ศ.{{ (date('Y')+543) }}
                            </option> --}}
                            @foreach ($indicator_year_view as $y)
                              <option value="{{$y->ind_year}}" {{ $y->ind_year == $year ? 'selected':''}}>
                                ตัวชี้วัด พ.ศ.{{$y->ind_year}}
                              </option>
                            @endforeach
                          </select>
                        </div>
          
                        <div class="col-md-4 form-group">
                          <label>รอบการประเมิน</label>
                          <select name="ddlRound" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlRound">
                            <option value="0">เลือกรอบการประเมิน</option>
                            <option value="1" {{$round==1 ?'selected' :''}}>การประเมินรอบที่ 1</option>
                            <option value="2" {{$round==2 ?'selected' :''}}>การประเมินรอบที่ 2</option>
                          </select>
                        </div>
                      </div>
          
                      <div class="row">
                        <div class="col-md-8 form-group">
                          <label>ตัวชี้วัด</label>
                          <input type="text" name="txtDetail" onfocusout="Validate(this.name, this.tagName)" class="form-control txtDetail">
                        </div>

                        <div class="col-md-4 form-group">
                          <label>คำอธิบายตัวชี้วัด</label>
                          <input type="file" name="txtDescFile" class="file-upload-default txtDescFile">
                          <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info file-name txtDescFileName" placeholder="ชื่อไฟล์" readonly>
                            <span class="input-group-append">
                              <button type="button" class="file-upload-browse btn btn-outline-primary" title="แนบไฟล์">
                                <i class="fa fa-paperclip"></i>
                              </button>
                            </span>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-2 form-group">
                          <label>น้ำหนักรอบที่ 1</label>
                          <input type="number" name="txtWeight1" class="form-control txtWeight1" {!! $round==1 ?'onfocusout="Validate(this.name, this.tagName)"' :'readonly' !!} value="">
                        </div>

                        <div class="col-md-2 form-group">
                          <label>น้ำหนักรอบที่ 2 (1)</label>
                          <input type="number" name="txtWeight21" class="form-control txtWeight21" {!! $round==1 ?'readonly' :'onfocusout="Validate(this.name, this.tagName)"' !!} value="">
                        </div>

                        <div class="col-md-2 form-group">
                          <label>น้ำหนักรอบที่ 2 (2)</label>
                          <input type="number" name="txtWeight22" class="form-control txtWeight22" {!! $round==1 ?'readonly' :'onfocusout="Validate(this.name, this.tagName)"' !!} value="">
                        </div>

                        <div class="col-md-2 form-group">
                          <label>น้ำหนักรอบที่ 2 (3)</label>
                          <input type="number" name="txtWeight23" class="form-control txtWeight23" {!! $round==1 ?'readonly' :'onfocusout="Validate(this.name, this.tagName)"' !!} value="">
                        </div>
                        
                        <div class="col-md-2 form-group">
                          <label>
                            num_1 
                            <!-- <br>Ex1. ข้อ1, 1.1, 1.2 ... num_1 => 1 
                            <br>Ex2. ข้อ2, 2.1, 2.2 ... num_1 => 2 -->
                          </label>
                          <input type="น้ำหนัก" name="txtNum1" class="form-control">
                        </div>

                        <div class="col-md-2 form-group">
                          <label>
                            num_2 
                            <!-- <br>Ex1.
                            <br>ข้อ 1 ใหญ่ (แบบมี Choice) => 0 
                            <br>ข้อ 1 ใหญ่ (แบบไม่มี Choice) => # 
                            <br>Ex2.
                            <br>ข้อ 1.1 => 1, ข้อ 1.2 => 2 
                            <br>ข้อ 2.1 => 1, ข้อ 2.2 => 2 -->
                          </label>
                          <input type="น้ำหนัก" name="txtNum2" class="form-control">
                        </div>
                      </div>    

                      <div class="row">
                        <div class="col-md-3 form-group">
                          <label>ประเภทคำตอบ</label>
                          <select name="ddlTypeAnswer" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlTypeAnswer">
                            <option value="0">เลือกประเภทคำตอบ</option>
                            <option value="value">Value</option>
                            <option value="checkbox">Checkbox</option>
                          </select>
                        </div>

                        <div class="col-md-3 form-group">
                          <label>สูตร</label>
                          <select name="ddlFormula" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlFormula">
                            <option value="0">เลือกสูตร</option>
                            <option value="(Number($('.txtAnswer0').val()) * 100) / Number($('.txtAnswer1').val())">(ข้อ1 x 100) / ข้อ2</option>
                            <option value="(Number($('.txtAnswer1').val()) * 100) / Number($('.txtAnswer2').val())">(ข้อ2 x 100) / ข้อ3</option>
                            <option value="Number($('.txtAnswer0').val()) / Number($('.txtAnswer1').val())">ข้อ1 / ข้อ2</option>
                            
                            <option value="-">ไม่มี</option>
                          </select>
                        </div>
                        
                        <div class="col-md-3 form-group">
                          <label>กลุ่มคะแนน</label>
                          <select name="ddlScoreGroup" onfocusout="Validate(this.name, this.tagName)" onchange="Validate(this.name, this.tagName)" class="form-control ddlScoreGroup">
                            <option value="0">กลุ่มคะแนน</option>
                            <option value="1">ประเมินประสิทธิผล (มิติภายนอก)</option>
                            <option value="2">การประเมินคุณภาพ (มิติภายนอก)</option>
                            <option value="3">ประเมินประสิทธิภาพ (มิติภายใน)</option>
                            <option value="4">การพัฒนาองค์การ (มิติภายใน)</option>
                          </select>
                        </div>
          
                        <div class="col-md-3 form-group">
                          <label>วันที่สิ้นสุดการรายงาน</label>
                          <input type="date" name="txtDateLimit" class="form-control">
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
              
              <div class="card">
                <div class="card-header" role="tab">
                  <h6 class="mb-0">
                    <a data-toggle="collapse" href="#div-indicator-choice" aria-expanded="true" aria-controls="div-indicator-choice">
                      <i class="card-icon mdi mdi-checkbox-multiple-marked-circle-outline"></i>
                      <u>ข้อย่อยตัวชี้วัด</u>
                    </a>
                  </h6>
                </div>

                <div id="div-indicator-choice" class="collapse show" role="tabpanel" data-parent="#accordion">
                  <div class="card-body">
                    <div class="div-tbl-choice table-responsive">
                      
                      {{-- <h5 class="text-danger remark" style="display:none;">กรุณากรอกข้อมูลให้ครบทุกช่อง</h5> --}}
                      <button type="button" class="btn btn-lg btn-primary w-100" onclick="AddChoice()">
                        <i class="fa fa-plus"></i> เพิ่มข้อย่อยตัวชี้วัด
                      </button>

                      <table class="tbl-choice table-bordered mt-3" width="100%">

                      </table>
                           
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="card-footer text-center">
            <button type="" onclick="ValidateClick()" class="btn btn-lg btn-success btnSave">
              <i class="far fa-save"></i> บันทึก
            </button>

            <button type="reset" class="btn btn-lg btn-danger">
              <i class="fa fa-undo"></i> ยกเลิก
            </button>
          </div>
        </div>
      </form>

    </div>
  </div>
  
@endsection

@section('js')
<script type="text/javascript">  
  function AddChoice(){ 
    // alert($('.tr-choice').length)
    let answer;
    answer  ='<tr class="tr-choice">';
    answer +=   '<td width="10%" class="text-center td-num"></td>';
    answer +=   '<td width="10%">';
    answer +=     '<input name="txtSection[]" class="form-control txtSection" placeholder="Section">';
    answer +=   '</td>';
    answer +=   '<td width="70%">';
    // answer +=     '<input name="txtChoice[]" class="form-control txtChoice">';
    answer +=     '<div class="input-group col-xs-12">';
    answer +=       '<input type="text" name="txtChoice[]" class="form-control txtChoice" placeholder="ข้อย่อย">';
    answer +=       '<span class="input-group-append">';
    answer +=         '<button type="button" class="btnDeleteChoice btn btn-outline-danger" title="">';
    answer +=           '<i class="fa fa-trash-alt"></i>';
    answer +=         '</button>';
    answer +=       '</span>';
    answer +=     '</div>';
    answer +=   '</td>';
    // answer +=   '<td width="10%" class="text-center">';
    // answer +=     '<a class="btnDeleteChoice"><i class="fa fa-trash-alt fa-lg text-danger"></i></a>';
    // answer +=   '</td>';
    answer +='</tr>';

    $('.tbl-choice').append(answer);
    Runnumber();
  }

  function Runnumber() { 
    let row_choice = $('.tr-choice').length;
    for(let i=0; i<row_choice; i++){
      $('.td-num').eq(i).text((i+1))
    }
  }

  $(".tbl-choice").on('click', ".btnDeleteChoice", function () {
    $(this).closest('tr').remove();
    Runnumber();
  });

  $(".tbl-choice").on('focusout', ".txtChoice", function () {
    if($(this).closest('.txtChoice').val() == '') {
      $(this).closest('.txtChoice').css('border-color', '#FF0000');
      $('.btnSave').attr('disabled',true);
    } else {
      $(this).closest('.txtChoice').css('border-color', '#2eb82e');
      $('.btnSave').attr('disabled',false)
    }
  });

  function ValidateChoice(name){
    if($('input[name="'+name+'"]').val() == '') {
      $('input[name="'+name+'"]').css('border-color', '#FF0000');
      $('.btnSave').attr('disabled',true);
    }else{
      $('input[name="'+name+'"]').css('border-color', '#2eb82e');
      $('.btnSave').attr('disabled',false)
    }
  }

  function Validate(name, tagname){
    if(tagname == 'SELECT') {
      console.log(tagname);
      if($('select[name="'+name+'"]').val() == 0) {
        $('select[name="'+name+'"]').css('border-color', '#FF0000');
        $('.btnSave').attr('disabled',true);
      }else{
        $('select[name="'+name+'"]').css('border-color', '#2eb82e');
        $('.btnSave').attr('disabled',false)
      }
    } 
    else if(tagname == 'INPUT') {
      console.log(tagname);
      if($('input[name="'+name+'"]').val() == '') {
        $('input[name="'+name+'"]').css('border-color', '#FF0000');
        $('.btnSave').attr('disabled',true);
      }else{
        $('input[name="'+name+'"]').css('border-color', '#2eb82e');
        $('.btnSave').attr('disabled',false)
      }
    }
  }

  function ValidateClick(){ 
    // if($('.txtDateLimit').val()== undefined){
    // alert($('.txtDateLimit').val())
    // }
    // ดักค่าบับันทึกข้อหลัก
    if($('.txtDetail').val()==''){
      $('.btnSave').attr('disabled',true);
      $('.txtDetail').css('border-color','red');
    }
    if($('.ddlRound').val()==1){
        if($('.txtWeight1').val()==''){
          $('.btnSave').attr('disabled',true);
          $('.txtWeight1').css('border-color','red');
        }
    } else {
        if($('.txtWeight21').val()==''){
          $('.btnSave').attr('disabled',true);
          $('.txtWeight21').css('border-color','red');
        }
        if($('.txtWeight22').val()==''){
          $('.btnSave').attr('disabled',true);
          $('.txtWeight22').css('border-color','red');
        }
        if($('.txtWeight23').val()==''){
          $('.btnSave').attr('disabled',true);
          $('.txtWeight23').css('border-color','red');
        }
    }

    if($('.ddlTypeOffice').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlTypeOffice').css('border-color','red');
    }
    if($('.ddlRound').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlRound').css('border-color','red');
    }
    if($('.ddlYear').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlYear').css('border-color','red');
    }

    if($('.ddlTypeAnswer ').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlTypeAnswer ').css('border-color','red');
    }
    if($('.ddlFormula ').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlFormula ').css('border-color','red');
    }
    if($('.ddlScoreGroup').val()==0){
      $('.btnSave').attr('disabled',true);
      $('.ddlScoreGroup').css('border-color','red');
    }

    //ดักค่าบันทึกข้อย่อย
    let row_choice = $('.tr-choice').length;
    if(row_choice == 0){
      // if(confirm('คุณยังไม่ได้เพิ่มข้อย่อย ต้องการเพิ่มข้อย่อยหรือไม่ ?')) {
      //   // $('.btnSave').attr('disabled',true);
      //   return false;
      // } else {
      //   // $('#frmInsert').submit();
      // }
    } else {
      for(i=0; i < row_choice; i++){
        if($('.txtChoice').eq(i).val()==''){
          $('.btnSave').attr('disabled',true);
          $('.txtChoice').eq(i).css('border-color','red');
        }
      }
    }
  }

  $('.ddlRound').change(function (e) { 
    e.preventDefault();
    let round = $(this).val();
    if(round == 1) {
      $('.txtWeight1').attr({'readonly':false});

      $('.txtWeight21').attr({'readonly':true}).css('border-color','#ccc').val('');
      $('.txtWeight22').attr({'readonly':true}).css('border-color','#ccc').val('');
      $('.txtWeight23').attr({'readonly':true}).css('border-color','#ccc').val('');
    }
    else if(round == 2) { 
      $('.txtWeight21').attr({'readonly':false, 'onfocusout':"Validate(this.name, this.tagName)"});
      $('.txtWeight22').attr({'readonly':false, 'onfocusout':"Validate(this.name, this.tagName)"});
      $('.txtWeight23').attr({'readonly':false, 'onfocusout':"Validate(this.name, this.tagName)"});
      
      $('.txtWeight1').attr({'readonly':true}).css('border-color','#ccc').val('');
    } 
    else {
      $('.txtWeight1').attr({'readonly':true}).css('border-color','#ccc').val('');
      $('.txtWeight21').attr({'readonly':true}).css('border-color','#ccc').val('');
      $('.txtWeight22').attr({'readonly':true}).css('border-color','#ccc').val('');
      $('.txtWeight23').attr({'readonly':true}).css('border-color','#ccc').val('');
    }
    
  });
</script>
@endsection