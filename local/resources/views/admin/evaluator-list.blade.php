@extends('layouts.master')

@section('title', 'ตั้งค่าผู้ตรวจและประเมินผล')
@section('css')
  <style type="">
    
  </style>
@stop
@section('content')
  <div class="row">
    <div class="col-12">
      <h2 class="form-inline">ตั้งค่าผู้ตรวจและประเมินผล
        <select class="form-control ml-2 ddlYear font-weight-bold-" onchange="getYear(this.value,$('.ddlRound').val())")">
          <option value="0">เลือกปี พ.ศ.</option>
          @foreach ($ind_year as $y)
            <option value="{{$y->ind_year}}" {{ $y->ind_year == $year ? 'selected':''}}>
              ตัวชี้วัด พ.ศ.{{$y->ind_year}}
            </option>
          @endforeach
        </select>
    
        <select name="" class="form-control ml-2 ddlRound" onchange="getYear($('.ddlYear').val(),this.value)">
          <option value="0">กรุณาเลือกรอบการประเมิน</option>
          <option value="1" {{$round==1 ?'selected' :''}}>การประเมินรอบที่ 1</option>
          <option value="2" {{$round==2 ?'selected' :''}}>การประเมินรอบที่ 2</option>
        </select>
      </h2>
    </div>

    {{-- @php
      $arr_ = [1,2,4,5];
      echo (string)in_array("3",$arr_);
    @endphp
    @for ($i=1; $i<6; $i++)
      <div class="icheck-square col-12">
        <input type="checkbox" name="chkIndicator[]" value="{{ $i }}" @if((string)in_array($i, $arr_)) {{'checked'}} @endif>
        <label>
          {{ 'detail : '.$i }}
        </label>
      </div>
      <br>
    @endfor --}}

  </div>

  <div class="row"> 
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <table class="tbl-indicator table-bordered">
            <thead class="text-center">
              <tr>
                <th width="5%">ลำดับ</th>
                <th width="%">ชื่อผู้ตรวจ</th>
                <th width="%">หน่วยงาน</th>
                <th width="%">สถานะ</th>
                <th width="5%">แก้ไข</th>
              </tr>
            </thead>  
            <tbody>
              @if(count($evaluators) > 0)
                @foreach ($evaluators as $i=>$evaluator)
                  <tr class="bg-secondary">
                    <td class="text-center">{{$i+1}}</td>
                    <td>
                      {{-- <a href="{{ url('admin/indicator/') }}" target="_blank"> --}}
                        {{$evaluator->name}}
                      {{-- </a> --}}
                    </td>
                    <td>
                      {{$evaluator->office_name}}
                    </td>
                    <td>
                      @if ($evaluator->user_group_id==1)
                        Admin
                      @else
                        ผู้ตรวจประเมินผล  
                      @endif
                    </td>
                    <td class="text-center"> 
                      <a href="#" class="btn btn-icons btn-warning" title="แก้ไข" data-toggle="modal" data-target=".frm-edit-{{$evaluator->id}}">
                        <i class="fa fa-edit"></i>
                      </a>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <th colspan="3" class="text-center">ยังไม่มีข้อมูล</th>
                </tr>
              @endif
            </tbody>
          </table> 

          @foreach ($evaluators as $i=>$evaluator)
            @php
              $arr_evaluate_group = explode(',',$evaluator->evaluate_group);
            @endphp
            <form id="frmEvalute{{$evaluator->id}}">
              {{ csrf_field() }}
              <div class="modal fade frm-edit-{{$evaluator->id}}">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h3 class="modal-title">
                        แก้ไขข้อมูลการตรวจประเมินผล {{$evaluator->name}} 
                      </h3>
                      <button class="btn btn-icons btn-rounded btn-outline-danger" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                      </button>
                    </div>

                    <div class="modal-body pt-2 px-5">
                      <div class="accordion basic-accordion" id="div-type-office" role="tablist">
                        <div class="row">
                          @foreach ($office_type as $type)
                            <div class="col-12">
                              <div class="card">
                                <div class="card-header" role="tab" id="heading{{$type->office_type_id}}">
                                  <h6 class="mb-0">
                                    <a data-toggle="collapse" href="#div-type{{$type->office_type_id}}" aria-expanded="false" aria-labelledby="heading{{$type->office_type_id}}" aria-controls="div-type{{$type->office_type_id}}">
                                      <i class="card-icon fa fa-home"></i>
                                      <u>ตัวชี้วัด{{$type->office_type_name}} พ.ศ.{{$year}} รอบการประเมินที่ {{$round}}</u>
                                    </a>
                                  </h6>
                                </div>
                                
                                <div id="div-type{{$type->office_type_id}}" class="collapse" role="tabpanel" data-parent="#div-type-office">
                                  <div class="card-body">
                                    <ul class="list-group">
                                      @php
                                        $indicators = DB::table('indicator')
                                          ->where(['type_id'=>$type->office_type_id,'ind_year'=>$year, 'ind_round'=>$round])
                                        ->get(); 
                                      @endphp
          
                                      @if(count($indicators) > 0)
                                        @foreach ($indicators as $i=>$ind)
                                          <li class="list-group-item">
                                            <div class="custom-control custom-checkbox">
                                              @if ($ind->num_02 != '#')
                                                <input type="checkbox" id="chkIndicator{{ $evaluator->id.'_'.$ind->ind_id }}" name="chkIndicator[]" value="{{ $ind->ind_id }}" @if((string)in_array($ind->ind_id, $arr_evaluate_group)) {{'checked'}} @endif class="custom-control-input">
                                              
                                                <label class="custom-control-label" for="chkIndicator{{ $evaluator->id.'_'.$ind->ind_id }}">
                                                  {{-- {{ $ind->ind_id }} :  --}}
                                                  {{ $ind->ind_detail }}
                                                </label>
                                              @else
                                                <label class="col-form-label py-0" for="chkIndicator{{ $evaluator->id.'_'.$ind->ind_id }}">
                                                  {{ $ind->ind_detail }}
                                                </label>
                                              @endif
                                            </div>
                                          </li>
                                        @endforeach
                                      @else
                                        <li class="list-group-item">
                                          <b>ยังไม่มีข้อมูลตัวชี้วัดของปี พ.ศ.{{$year}} รอบการประเมินที่ {{$round}}</b>
                                        </li>
                                      @endif
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                          @endforeach
                        </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                      <button type="button" onclick="editEvalute({{ $evaluator->id }}, {{$round}})" class="btn btn-lg btn-success" title="บันทึก & เปลี่ยนแปลง">
                        <i class="far fa-save"></i> บันทึก & เปลี่ยนแปลง
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          @endforeach
        </div>
      </div>
    </div>
  </div>
  
  {{-- <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
            <div class="accordion basic-accordion" id="div-type-office" role="tablist">
              <div class="row">
                @foreach ($office_type as $type)
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header" role="tab" id="heading{{$type->office_type_id}}">
                        <h6 class="mb-0">
                          <a data-toggle="collapse" href="#div-type{{$type->office_type_id}}" aria-expanded="false" aria-labelledby="heading{{$type->office_type_id}}" aria-controls="div-type{{$type->office_type_id}}">
                            <i class="card-icon fa fa-home"></i><u>ตัวชี้วัด{{$type->Name_office_type}}</u>
                          </a>
                        </h6>
                      </div>
                      <div id="div-type{{$type->office_type_id}}" class="collapse" role="tabpanel" data-parent="#div-type-office">
                        <div class="card-body">
                          <ul class="list-group">
                            @php
                              $indicators = App\Indicator::where(['type_id'=>$type->office_type_id,'ind_year'=>$year])->get();
                            @endphp
  
                            @if(count($indicators) > 0) 
                              @foreach ($indicators as $i => $ind)
                                <li class="list-group-item">
                                  <a href = "{{ url('evaluation').'/'.$ind->ind_id.'/3' }}"> {{$ind->ind_detail}}</a>
                                </li>
                              @endforeach
                            @else
                              <li class="list-group-item">
                                <a>ยังไม่มีข้อมูลตัวชี้วัดของปี พ.ศ.{{$year}}</a>
                              </li>
                            @endif
                        </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </ul>
        </div>
      </div>
    </div>
  </div> --}}
@endsection

@section('js')
<script type="text/javascript">  
  function getYear(year,round) {
    if(year==0 && round==0){ //console.log(1);
      window.location.href='{{url('admin/evaluator')}}';
    } else if(year!=0 && round!=0){ //console.log(2);
      window.location.href= '{{url("admin")}}'+'/'+year+'/'+round+'/evaluator';
    }
  }

  function editEvalute(user_id, round){ 
    // console.log($('#frmEvalute'+user_id).serialize());
    
    $.ajax({
      type: 'post',
      url: '{{url('admin/editEvalute')}}',
      data: $('#frmEvalute'+user_id).serialize() + '&user_id='+user_id + '&round='+round,
      success: function (msg) {
        alert(msg);
        console.log(msg);
        // location.reload();
      }
    });
  }
</script>
@endsection