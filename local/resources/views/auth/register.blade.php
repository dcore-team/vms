<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>เข้าสู่ระบบ | {{ Config::get('app.name') }}</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ url('assets-pn/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ url('assets-pn/vendors/iconfonts/puse-icons-feather/feather.css') }}">
  <link rel="stylesheet" href="{{ url('assets-pn/vendors/iconfonts/fontawesome-5.7.0/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ url('assets-pn/vendors/css/vendor.bundle.base.css') }}">
  <link rel="stylesheet" href="{{ url('assets-pn/vendors/css/vendor.bundle.addons.css') }}">

  <link rel="stylesheet" href="{{ url('assets-pn/css/shared/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ url('img/icon.png') }}" />
</head>

<body>
  <div class="card">
  <form method="POST" action="{{ route('register') }}">
    @csrf
    <div class="card-body">
      
      <div class="row form-group">
        <label class="col-form-label col-lg-4">ชื่อ-นามสกุล <b class="text-danger">*</b></label>
        <div class="col-lg-8">
          <input name="txtName" value="{{ old('txtName') }}" class="form-control" placeholder="" required autofocus>
        </div>
      </div>  
      <div class="row form-group">
        <label class="col-form-label col-lg-4">ชื่อผู้ใช้งาน <b class="text-danger">*</b></label>
        <div class="col-lg-8">
          <input name="username" value="{{ old('username') }}"class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="" required>
          @if ($errors->has('username'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('username') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="row form-group">
        <label class="col-form-label col-lg-4">รหัสผ่าน <b class="text-danger">*</b></label>
        <div class="col-lg-8">
          <input type="password" value="sp001-75"  name="password" placeholder="Password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="" required>
          @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif {{--tr01-17--}}
        </div>
      </div>
      <div class="row form-group">
        <label class="col-form-label col-lg-4">ยืนยันรหัสผ่าน <b class="text-danger">*</b></label>
        <div class="col-lg-8">
          <input type="password" name="password_confirmation" id="password-confirm" class="form-control" placeholder="" required>
        </div>
      </div>
      <div class="row form-group">
        <label class="col-form-label col-lg-4">เบอร์โทร</label>
        <div class="col-lg-8">
          <input name="txtTel" value="{{ old('txtTel') }}" class="form-control" placeholder=""> 
        </div>
      </div>
      <div class="row form-group">
        <label class="col-form-label col-lg-4">อีเมล์</label>
        <div class="col-lg-8">
          <input type="email" name="email" value="{{ old('email') }}" placeholder="E-Mail" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
          @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif 
        </div>
      </div>
      <div class="row form-group">
        <label class="col-form-label col-lg-4">หน่วยงาน</label>
        <div class="col-lg-8">
          <input name="txtOffice" value="{{ old('txtOffice') }}" class="form-control{{ $errors->has('office') ? ' is-invalid' : '' }}" placeholder="" required> 
          @if ($errors->has('office'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('office') }}</strong>
            </span>
          @endif
        </div>
      </div>
      <div class="row form-group">
        <label class="col-form-label col-lg-4">ประเภทหน่วยงาน</label>
        <div class="col-lg-8">
          <select name="ddlType">
            <option value="1" @if(old('ddlType')=='1'){{'selected'}}@endif>1</option>
            <option value="2" @if(old('ddlType')=='2'){{'selected'}}@endif>2</option>
          </select>
        </div>
      </div>
      <div class="row form-group">
        <label class="col-form-label col-lg-4">เขต</label>
        <div class="col-lg-8">
          <select name="ddlZone">
            <option value="1" @if(old('ddlZone')=='1'){{'selected'}}@endif>1</option>
            <option value="2" @if(old('ddlZone')=='2'){{'selected'}}@endif>2</option>
          </select>
        </div>
      </div>
      <div class="row form-group">
        <label class="col-form-label col-lg-4">ประเภทผู้ใช้งาน</label>
        <div class="col-lg-8">
          <select name="ddlGroup">
            <option value="admin" @if(old('ddlGroup')=='admin'){{'selected'}}@endif>1</option>
            <option value="user" @if(old('ddlGroup')=='user'){{'selected'}}@endif>2</option>
          </select>
        </div>
      </div>
        
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-lg btn-block btn-primary">สมัครสมาชิก</button>
      <div class="d-flex justify-content-center form-group ">
          <a href="{{ route('login') }}">เข้าสู่ระบบ</a>
      </div>
    </div>
  </form>
  </div>  

<script src="{{ url('assets-pn/vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ url('assets-pn/vendors/js/vendor.bundle.addons.js') }}"></script>

<script src="{{ url('assets-pn/js/shared/off-canvas.js') }}"></script>
<script src="{{ url('assets-pn/js/shared/hoverable-collapse.js') }}"></script>
<script src="{{ url('assets-pn/js/shared/settings.js') }}"></script>
<script src="{{ url('assets-pn/js/shared/todolist.js') }}"></script>

</body>
</html>  