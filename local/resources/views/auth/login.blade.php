<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>เข้าสู่ระบบ | {{ Config::get('app.name') }}</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/puse-icons-feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/fontawesome-5.8.1/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.addons.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/shared/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/icheck/skins/all.css') }}">

    <link rel="shortcut icon" href="{{ url('').'/'.Config::get('app.icon') }}" />
  </head>

  <body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">

        <div class="row w-100">

          {{-- <div class="col-md-12 text-center ">
            <h4 class="text-white">
              รหัสผ่านปิดการใช้งานชั่วคราวเนื่องจาก
            </h4>
            <h4 class="text-white">
              อยู่ระหว่างการประเมินผลการอุทธรณ์ตัวชี้วัด หากแล้วเสร็จจะแจ้งให้ทราบอีกครั้ง
              @for ($i=1; $i<=9; $i++)
                @php
                  $pass = 'djop'.$i;
                  // $hash = password_hash($pass, PASSWORD_DEFAULT);
                  $hash = bcrypt($pass);
                @endphp

                @if (password_verify($pass, $hash))
                  {{'ถูก : '.$pass }}
                  {{$hash}}<br>
                @else
                  {{'ผิด'}} <br>
                @endif
              @endfor

              @for ($i=10; $i<=10; $i++)
                @php
                  $pass = 'djop'.$i;
                  // $hash = password_hash($pass, PASSWORD_DEFAULT);
                  $hash = bcrypt($pass);
                @endphp

                @if (password_verify($pass, $hash))
                  {{'ถูก : '.$pass }}
                  {{$hash}}<br>
                @else
                  {{'ผิด'}} <br>
                @endif
              @endfor
            </h4>
          </div> --}}
          
          {{-- <div style="background-color: cyan">
            @for ($i = 0; $i < 6; $i++)
              <div>
                <input type="checkbox" name="buy-item" value="{{ $i }}">
              </div>
            @endfor

            <div>
              <button id="btn">ok</button>
            </div>
          </div> --}}

          <div class="col-lg-4 col-md-5 col-6 mx-auto">

            <div class="auto-form-wrapper pt-3">
              <div class="text-center px-5">
                <img src="{{ url('').'/'.Config::get('app.logo') }}" style="width:50%; height:auto;">
                <!-- <p class="text-muted font-weight-bold mt-2">เข้าใช้งานระบบโดยใส่ข้อมูล ชื่อผู้ใช้งาน,รหัสผ่าน</p> -->
              </div>

              @if($errors->all())
                <div class="alert alert-danger alert-dismissible">
                  <a class="close" data-dismiss="alert">&times;</a>
                  <b>!</b> {{ $errors->first() }}
                </div>
              @endif
              <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                  <label class="label">ชื่อผู้ใช้งาน</label>
                  <div class="input-group">

                    <input type="text" name="txtUser" value="{{old('txtUser')}}" class="form-control{{ $errors->has('txtUser') ? ' is-invalid' : '' }}" placeholder="ชื่อผู้ใช้งาน" required autofocus>
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-account-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label">รหัสผ่าน</label>
                  <div class="input-group">
                    <input type="password" name="txtPass" value="" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="รหัสผ่าน" required>
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-lock-outline"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group text-center">
                  <button class="btn btn-lg btn-block btn-primary submit-btn">
                    <i class="fa fa-lg fa-sign-in-alt"></i> เข้าสู่ระบบ {{ old('remember') }}
                  </button>
                  {{-- <a href="{{ route('register') }}">adduser</a> --}}
                </div>
                <div class="form-group d-flex justify-content-between">

                  <div class="icheck-square">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ?'checked' :'' }} class="form-check-input"> 
                    <label for="remember"> Keep me signed in </label>
                  </div>

                  <div class="form-check form-check-flat mt-0">
                    <label class="form-check-label">
                      
                    </label>
                  </div>
                </div>
              </form>
            </div>

            <p class="footer-text text-center">© 2019 DCore System Integrator. All rights reserved.</p>

          </div>

        </div>

      </div>
    </div>
  </div>
  <script src="{{ asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>
  <script src="{{ asset('assets/vendors/js/vendor.bundle.addons.js') }}"></script>

  <script src="{{ asset('assets/js/shared/off-canvas.js') }}"></script>
  <script src="{{ asset('assets/js/shared/hoverable-collapse.js') }}"></script>
  <script src="{{ asset('assets/js/shared/misc.js') }}"></script>
  <script src="{{ asset('assets/js/shared/settings.js') }}"></script>
  <script src="{{ asset('assets/js/shared/iCheck.js') }}"></script>
</body>
</html>


<script>
    $('#btn').click(function (e) { 
      e.preventDefault();
      var result = '';
      let arr_ = [];

      $.each( $("input[name='buy-item']:checked"), function() {
        result += $(this).val();
        arr_.push($(this).val());
      });
        $(selector).data({
          key: value
          
        });
      console.log(arr_);
    });

  </script>
{{-- psd001 ery5k8
<button class="btn btn-lg btn-danger" style="background-image: linear-gradient(120deg, #ff0000, #ff5623, #f1a93e);"><i class="fa fa-times"></i> ยกเลิก</button>
<input type="text" name="email" value="{{old('email')}}" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="ชื่อผู้ใช้งาน" required autofocus>

@for ($i=1; $i<=16; $i++)
  @php
    $pass = 'dep'.$i;
    // $hash = password_hash($pass, PASSWORD_DEFAULT);
    $hash = bcrypt($pass);
  @endphp

  @if (password_verify($pass, $hash))
    {{'ถูก : '.$pass }} = {{ $hash }} <br>
  @else
    {{'ผิด'}} <br>
  @endif
@endfor

--}}
