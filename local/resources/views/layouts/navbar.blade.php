<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
	<div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
		<a class="navbar-brand brand-logo" href="{{ url('/home') }}">
			<img src="{{ url(Config::get('app.logo')) }}" style="width:%; height:55px;">
		</a>
		<a class="navbar-brand brand-logo-mini" href="{{ url('/home') }}">
			<img src="{{ url(Config::get('app.icon')) }}" class="rounded-circle1">
		</a>
	</div>
	<div class="navbar-menu-wrapper d-flex align-items-center">
		<button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
			<span class="mdi mdi-menu"></span>
		</button>
		<ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex pl-0">
			<li class="nav-item">
				<a class="nav-link" style="font-size: 1em;">
					<i class="mdi mdi-calendar-clock"></i>
					<span id="dateThai"></span>&nbsp;&nbsp;&nbsp;
					<span id="diffTime"></span>
				</a>
      </li>
    </ul>
    
    <ul class="navbar-nav navbar-nav-right">
			@if ($data_user_view->user_group_id != '203')
				<li class="nav-item dropdown">
					<a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
						<i class="mdi mdi-bell-outline"></i>
						<span class="count bg-warning notification "></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="messageDropdown">

            <a class="dropdown-item py-3">
              <p class="mb-0 font-weight-medium float-left">
                แจ้งผลคะแนน KPI 
                <span class="notification">
                  
                </span>
              </p>
            </a>
            <div class="dropdown-divider"></div>
            
              <a href="{{ url('score') }}"class="dropdown-item preview-item notification" style="{{ " > 0 ? :display: none !important;" }}">
                <div class="preview-item-content flex-grow py-2">
                  <!-- <p class="preview-subject ellipsis font-weight-medium text-dark">{{ '' }}</p> -->
                  <p class="font-weight-light small-text ellipsis">
                    ตรวจสอบคะแนน
                  </p>
                </div>
              </a>
					</div>
				</li>
			@endif

			<li class="nav-item dropdown d-none d-xl-inline-block">
				<a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
					<span class="profile-text">
						{{ Auth::User()->username }} / 
						{{ '' }}
					</span>
					<img class="img-xs rounded-circle" src="{{ url('').'/'.Config::get('app.icon') }}">
				</a>
				<div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="">
					<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item">
						<i class="mdi mdi-logout-variant"></i>ออกจากระบบ
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
			</li>
    </ul>
    
		<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
			<span class="icon-menu"></span>
		</button>
	</div>
</nav>
