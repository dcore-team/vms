
<footer class="footer">
  <div class="container-fluid clearfix">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">© 2019. All rights reserved.</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">By DCore System Integrator
      <i class="mdi mdi-heart text-danger"></i>
    </span>
  </div>
</footer>