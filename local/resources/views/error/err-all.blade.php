<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ $status_code }} | {{ Config::get('app.name') }}</title>
    <link rel="shortcut icon" href="{{ url('img/icon.png') }}" />

    <link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/iconfonts/puse-icons-feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.addons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/shared/style.css') }}">

  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center text-center error-page {{ $bg_page }}">
          <div class="row flex-grow">
            <div class="col-lg-7 mx-auto text-white">
              <div class="row align-items-center d-flex flex-row">
                <div class="col-lg-6 text-lg-right pr-lg-4">
                  <h1 class="display-1 mb-0">{{ $status_code }}</h1>
                </div>
                <div class="col-lg-6 error-page-divider text-lg-left pl-lg-4">
                  <h2>SORRY!</h2>
                  <h3 class="font-weight-light">{{ $msg_err }}</h3>
                </div>
              </div>
              <div class="row mt-5">
                <div class="col-12 text-center mt-xl-2">
                  <a class="text-white font-weight-medium" href="{{ url('/home') }}">Back to home</a>
                </div>
              </div>
              <div class="row mt-5">
                <div class="col-12 mt-xl-2">
                  <p class="text-white font-weight-medium text-center">Copyright &copy; 2019 All rights reserved.</p>
                </div>
              </div>
            </div>

            {{-- <div class="col-lg-9 mx-auto text-white">
              <div class="row align-items-center d-flex flex-row">
                <div class="col-md-10 mx-auto">
                  @php
                    $arr_msg = explode('/', $msg_err);
                    
                  @endphp
                  <h3>
                    @foreach ($arr_msg as $msg)
                      {{ $msg }} <br>
                    @endforeach
                  </h3>

                  <h3 class="font-weight-light">{{ $msg_err2 }}</h3>
                </div>
              </div>
              <div class="row mt-5">
                <div class="col-12 text-center mt-xl-2">
                  <a class="text-white font-weight-medium" href="{{ url('/home') }}">กลับหน้าหลัก</a>
                </div>
              </div>
              <div class="row mt-5">
                <div class="col-12 mt-xl-2">
                  <p class="text-white font-weight-medium text-center">Copyright &copy; 2019 All rights reserved.</p>
                </div>
              </div>
            </div> --}}

          </div>
        </div>
      </div>
    </div>
  </body>
</html>
