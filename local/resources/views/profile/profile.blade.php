@extends('layouts.master')

@section('title', 'ข้อมูลส่วนตัว')
@section('css')
  <style>

  </style>
@stop
@section('content')
<div class="row">
  <div class="col-md-12">
    <h1>ข้อมูลส่วนตัว</h1>
  </div>
</div>
<div class="row profile-page">
	<div class="col-md-12">
    <div class="card">

        <form method="POST" action="{{ url('profile') }}">
          @csrf
          <div class="card-body">
            <div class="profile-header text-white">
              <div class="d-flex justify-content-around">
                <div class="profile-info d-flex align-items-center">
                  <img class="rounded-circle img-lg" src="{{ url('img/icon.png') }}">
                  <div class="wrapper pl-4">
                    <p class="profile-user-name">{{ $data_users->users_name }} ({{ $data_users->Name_office }})</p>
                    <div class="wrapper d-flex align-items-center">
                      <p class="profile-user-designation">ประเภทหน่วยงาน : {{ $data_users->Name_office_type }}</p>
                    </div>
                  </div>
                </div>
                <div class="details">
                  <div class="detail-col">
                    <p>Projects</p>
                    <p>130</p>
                  </div>
                  <div class="detail-col">
                    <p>Projects</p>
                    <p>130</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="profile-body">
              <div class="row form-group">
                <label class="col-form-label col-lg-3">ชื่อ-นามสกุล <b class="text-danger">*</b></label>
                <div class="col-lg-6">
                  <input name="txtName" value="{{ $data_users->users_name }}" class="form-control{{$errors->has('txtName')?' is-invalid':''}}" maxlength="200" autofocus>
                  @if ($errors->has('txtName'))
                    <span class="invalid-feedback" role="alert">
                      <b>{{ $errors->first('txtName') }}</b>
                    </span>
                  @endif
                </div>
              </div>
              <div class="row form-group">
                <label class="col-form-label col-lg-3">เบอร์โทร</label>
                <div class="col-lg-6">
                  <input name="txtTel" value="{{ Auth::user()->tel }}" class="form-control{{$errors->has('txtTel')?' is-invalid':''}}" placeholder="" min="9" maxlength="10">
                  @if ($errors->has('txtTel'))
                    <span class="invalid-feedback" role="alert">
                      <b>{{ $errors->first('txtTel') }}</b>
                    </span>
                  @endif
                </div>
              </div>
              <div class="row form-group">
                <label class="col-form-label col-lg-3">E-Mail</label>
                <div class="col-lg-6">
                  <input type="email" name="txtEmail" value="{{ Auth::user()->email }}" placeholder="E-Mail" id="email" class="form-control{{$errors->has('txtEmail')?' is-invalid':''}}">
                  @if ($errors->has('txtEmail'))
                    <span class="invalid-feedback" role="alert">
                      <b>{{ $errors->first('txtEmail') }}</b>
                    </span>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer text-center">
            <button class="btn btn-lg btn-warning"><i class="fa fa-edit"></i> แก้ไข</button>
            <button type="reset" class="btn btn-lg btn-danger"><i class="fa fa-times"></i> ยกเลิก</button>
          </div>
        </form>

    </div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  @if(\Session::has('success'))
    alert('{{ \Session::get('success') }}');
  @endif
</script>
@endsection

              {{-- @if($errors->all())
                <div class="alert alert-danger">
                    {{ $errors->first() }}
                </div>
              @endif --}}
              {{-- <div class="row form-group">
                <label class="col-form-label col-lg-4">รหัสผ่าน <b class="text-danger">*</b></label>
                <div class="col-lg-8">
                  <input type="password" value="sp001-75"  name="password" placeholder="Password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="" required>
                  @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="row form-group">
                <label class="col-form-label col-lg-4">ยืนยันรหัสผ่าน <b class="text-danger">*</b></label>
                <div class="col-lg-8">
                  <input type="password" name="password_confirmation" id="password-confirm" class="form-control" placeholder="" required>
                </div>
              </div> --}}
              {{-- <div class="row form-group">
                <label class="col-form-label col-lg-4">ประเภทผู้ใช้งาน</label>
                <div class="col-lg-8">
                  <select name="ddlGroup">
                    <option value="admin" @if(old('ddlGroup')=='admin'){{'selected'}}@endif>1</option>
                    <option value="user" @if(old('ddlGroup')=='user'){{'selected'}}@endif>2</option>
                  </select>
                </div>
              </div> --}}
