@extends('layouts.master')

@section('title', 'ข้อมูลลูกบ้าน')
@section('css')
  <style>

  </style>
@stop
@section('content')
<div class="row"> 
  <div class="col-md-8">
		<h2 class="form-inline">
        ข้อมูลลูกบ้าน
    </h2>
  </div>
  

  <div class="col-md-4 text-right">
    <a href="{{ url('setting/resident-create') }}" class="btn btn-lg btn-success">
      <i class="fa fa-plus"></i> เพิ่มข้อมูล
    </a>
  </div>
</div>

<div class="row">
	<div class="col-md-12">
    <div class="card grid-margin">
      <div class="card-body">
        <h4 class="font-weight-bold">
            <u>
              
            </u>
        </h4>
        
        <table class="tbl-indicator table-bordered tbl-resident">
          <thead class="text-center">
            <tr>
              <th width="5%">ลำดับ</th>
              <th width="%">ชื่อ</th>
              <th width="%">ที่อยู่</th>
              <th width="15%">รหัสประจำตัวประชาชน</th>
              <th width="15%">เบอร์โทรศัพท์</th>
              {{-- <th width="%">รูปภาพ (people)</th>
              <th width="%">รูปภาพ (car)</th> --}}
              <th width="10%">ดำเนินการ</th>
            </tr>
          </thead>

          <tbody>
            
          </tbody>
        </table>

      </div>
    </div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  var resident_main = (function () {
    var datatable_resident1;

    var datatable_resident = function () {
      datatable_resident1 = $('.tbl-resident').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        // order: [[ 1, 'asc' ]],
        iDisplayLength: 20,
        aLengthMenu: [
          [20, 30, 40, 50, -1], 
          [20, 30, 40, 50, "ทั้งหมด"]
        ], 
        language: {
          search: "" 
        },
  
        ajax: {
          type: "GET",
          url: "{{ url('setting/get-resident') }}",
          data: function (d) {
            // d.year = $('select[name=ddlYear]').val();
            // d.round = $('select[name=ddlRound]').val();
            // console.log(d);
          },
        },
        columns: [
          { data: 'id'},
          { data: 'name' },
          { data: 'address' },
          { data: 'card_id' },
          { data: 'tel' },
          { data: 'id' },
        ],
        columnDefs: [
          { 
            targets: 0, "className":"text-center",
            render: function (data, type, row, meta) {
              // console.log(meta);
              return (meta.row + meta.settings._iDisplayStart + 1);
            } 
          },

          // { 
          //   targets: 2,
          //   searchable: false,
          //   className: 'text-center',
  
          //   render: function (data, type, row, meta) { 
          //     return '<img class="img" src="{{ asset("local/public/image_people") }}'+'/'+data+' ">';
          //   } 
          // },

          {
            targets: -1,
            searchable: false,
            className: 'text-center',
  
            render: function (data, type, row, meta) { //`
              return '<a class="btn btn-icons btn-warning" href="{{ url("setting") }}'+'/'+data+'/resident-edit" title="ดู/แก้ไขข้อมูล"><i class="fa fa-edit"></i></a>\
                <button class="btn btn-icons btn-danger" onclick="deleteResident('+data+')" title="ลบข้อมูล"><i class="fa fa-trash-alt"></i></button>';
            }
          }
        ],
      });
    }
  
    deleteResident = function(id) { 
      if(confirm('ต้องการลบข้อมูลหรือไม่ ?')) {
        $.ajax({
          type: "POST",
          url: `{{ url('setting/${id}/resident-delete') }}`,
          // data: {user_id: id},
          
          success: function (res) {
            console.log(res);
            alert(res.msg)
            datatable_resident1.ajax.reload();
            // location.reload();
          },
          error: function (err) {
            console.log(err.responseJSON);
          }
        });
      }
    }

    return {
      init: function () {
        datatable_resident();
      }
    }

  })()

  $(document).ready(function () {
    resident_main.init();
  });
</script>
@endsection
