@extends('layouts.master')

@section('title', 'แก้ไขข้อมูลลูกบ้าน')
@section('css')
  <style>

  </style>
@stop
@section('content')
<div class="row"> 
  <div class="col-md-12">
		<h2 class="form-inline">
        แก้ไขข้อมูลลูกบ้าน
    </h2>
  </div>
</div>

<div class="row">
  <div class="col-md-12">

    <form id="frmEdit"> 
      <div class="card">
        <div class="card-body pb-3">

          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4 form-group">
                  <label>ชื่อ-นามสกุล</label>
                  <input type="text" name="txtName" value="{{ $resident->name }}" onfocusout="Validate(this.name, this.tagName)" class="form-control txtName">
                </div>
  
                <div class="col-md-4 form-group">
                  <label>รหัสประจำตัวประชาชน</label>
                  <input type="number" name="txtCardID" value="{{ $resident->card_id }}" maxlength="13" class="form-control txtCardID">
                </div>
  
                <div class="col-md-4 form-group">
                  <label>เบอร์โทรศัพท์</label>
                  <input type="number" name="txtTel" value="{{ $resident->tel }}" maxlength="10" class="form-control txtTel">
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-8 form-group">
                  <label>ที่อยู่</label>
                  <textarea name="txtAddress" onfocusout="Validate(this.name, this.tagName)" class="form-control txtAddress">{{ $resident->address }}</textarea>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 form-group">
                  <label>รูปภาพ (ลูกบ้าน)</label>
                  <input type="file" name="image_people" class="dropify" accept="image/*" data-default-file="{{ asset('local/public/resident/people/'.$resident->image_people) }}">
                </div>
  
                <div class="col-md-6 form-group">
                  <label>รูปภาพ (รถ)</label>
                  <input type="file" name="image_car" class="dropify" accept="image/*" data-default-file="{{ asset('local/public/resident/car/'.$resident->image_car) }}">
                </div>
              </div>

            </div>
          </div>

        </div>

        <div class="card-footer text-center">
          <button type="button" onclick="updateResident({{ $resident->id }})" class="btn btn-lg btn-success btnEdit">
            <i class="far fa-edit"></i> บันทึก & เปลี่ยนแปลง
          </button>

          <button type="reset" class="btn btn-lg btn-danger">
            <i class="fa fa-undo"></i> ยกเลิก
          </button>
        </div>
      </div>
    </form>

  </div>
</div>
@endsection

@section('js')
<script>
  function updateResident(id) { 
    $.ajax({
      type: "POST",
      url: `{{ url('setting/${id}/resident-update') }}`,
      data: $('#frmEdit').serialize(),
      
      success: function (res) {
        console.log(res);

        if(res.error > 0){
          alert(res.msg.join( "\n"));
        } else {
          alert(res.msg);
          location.href="{{ url('setting/resident') }}";
        }
      },
      error: function (err) {
        console.log(err.responseJSON);
      }
    });
  }
</script>
@endsection
